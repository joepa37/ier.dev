<?php

use yii\helpers\Html;

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('core/auth', 'Password recovery');

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);
?>

<div class="login-box">
    <div class="login-logo">
        <a href="/"><b>IER</b>School</a>
    </div>
    <div class="login-box-body">
        <b>
            <p class="login-box-msg">Password recovery</p>
        </b>

        <div class="form-group has-feedback">
            <div class="alert alert-success text-center">
                <?= Yii::t('core/auth', 'Check your E-mail for further instructions') ?>
            </div>
        </div>


        <div class="row registration-block">
            <div class="col-sm-<?= $col12 ?> ">
                <?= Html::a(Yii::t('core/auth', "Return"), ['/admin']) ?>
            </div>
        </div>

    </div>
</div>
