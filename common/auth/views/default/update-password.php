<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;

/**
 * @var yii\web\View $this
 * @var common\auth\models\forms\UpdatePasswordForm $model
 */
$this->title = Yii::t('core/auth', 'Update Password');
$this->params['breadcrumbs'][] = $this->title;

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);
?>
<div id="update-wrapper">
    <div class="box box-primary">
        <div class="panel-default">
            <div class="row">
                <div class="col-md-<?= $col6 ?> col-md-offset-<?= $col3 ?>">
                    <div class="panel-body">
                        <?php
                        \yii\widgets\Pjax::begin([
                            'id' => 'update-form-pjax',
                        ])
                        ?>

                        <?php $form = ActiveForm::begin([
                            'id' => 'update-form',
                            'options' => ['autocomplete' => 'off'],
                            'validateOnBlur' => false,
                        ]) ?>

                        <?php if ($model->scenario != 'restoreViaEmail'): ?>
                            <?= $form->field($model, 'current_password')->passwordInput(['maxlength' => 255]) ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 255]) ?>

                        <?= $form->field($model, 'repeat_password')->passwordInput(['maxlength' => 255]) ?>

                        <?= Html::submitButton(Yii::t('core', 'Update'), ['class' => 'btn btn-lg btn-primary btn-block']) ?>

                        <?php ActiveForm::end() ?>

                        <?php \yii\widgets\Pjax::end() ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>