<?php

/**
 * @var yii\web\View $this
 */

$this->title = Yii::t('core/auth', 'Update Password Success');
$this->params['breadcrumbs'][] = $this->title;

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);
?>
<div class="change-own-password-success">
    <div class="box box-success">
        <div class="panel-default">
            <div class="row">
                <div class="col-md-<?= $col6 ?> col-md-offset-<?= $col3 ?>">
                    <div class="panel-body">
                        <div class="alert alert-success text-center">
                        <?= Yii::t('core/auth', 'Password has been updated') ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
</div>
