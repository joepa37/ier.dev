<?php

use common\auth\assets\AvatarAsset;
use common\auth\assets\AvatarUploaderAsset;
use common\auth\widgets\AuthChoice;
use common\widgets\ActiveForm;
use common\helpers\Html;
use common\helpers\CoreHelper;
use yii\helpers\Url;

/**
 * @var yii\web\View $this
 * @var common\auth\models\forms\SetEmailForm $model
 */

$this->title = Yii::t('core/user', 'User Profile');
$this->params['breadcrumbs'][] = $this->title;
if(\common\models\User::hasPermission('changeOwnPassword')){
    $this->params['buttons'] = [
        \yii\helpers\Html::a(Yii::t('core/auth', 'Update Password'), ['/auth/default/update-password'], ['class' => 'btn btn-primary btn-sm pull-right'])
    ];
}

AvatarUploaderAsset::register($this);
AvatarAsset::register($this);

$col12 = $this->context->module->gridColumns;
$col9 = (int) ($col12 * 3 / 4);
$col6 = (int) ($col12 / 2);
$col3 = (int) ($col12 / 4);

?>


<div class="profile-index">
    <div class="box box-primary">
        <div class="panel-body">
            <div class="row">
                <div class="col-md-<?= $col3 ?>">

                    <div class="image-uploader">
                        <?php
                        ActiveForm::begin([
                            'method' => 'post',
                            'action' => Url::to(['/auth/default/upload-avatar']),
                            'options' => ['enctype' => 'multipart/form-data', 'autocomplete' => 'off'],
                        ])
                        ?>

                        <?php $avatar = ($userAvatar = Yii::$app->user->identity->getAvatar('large')) ? $userAvatar : AvatarAsset::getDefaultAvatar('large') ?>
                        <div class="image-preview" data-default-avatar="<?= $avatar ?>">
                            <img src="<?= $avatar ?>"/>
                        </div>
                        <div class="image-actions">
                    <span class="btn btn-primary btn-file"
                          title="<?= Yii::t('core/auth', 'Change profile picture') ?>" data-toggle="tooltip"
                          data-placement="left">
                        <i class="fa fa-folder-open fa-lg"></i>
                        <?= Html::fileInput('image', null, ['class' => 'image-input']) ?>
                    </span>

                            <?=
                            Html::submitButton('<i class="fa fa-save fa-lg"></i>', [
                                'class' => 'btn btn-primary image-submit',
                                'title' => Yii::t('core/auth', 'Save profile picture'),
                                'data-toggle' => 'tooltip',
                                'data-placement' => 'top',
                            ])
                            ?>

                            <span class="btn btn-primary image-remove"
                                  data-action="<?= Url::to(['/auth/default/remove-avatar']) ?>"
                                  title="<?= Yii::t('core/auth', 'Remove profile picture') ?>" data-toggle="tooltip"
                                  data-placement="right">
                        <i class="fa fa-remove fa-lg"></i>
                    </span>
                        </div>
                        <div class="upload-status"></div>

                        <?php ActiveForm::end() ?>
                    </div>

                </div>

                <?php
                $form = ActiveForm::begin([
                    'id' => 'user',
                    'validateOnBlur' => false,
                ])
                ?>

                <div class="col-md-<?= $col9 ?>">

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <?= $form->field($model, 'username')->textInput(['maxlength' => 255, 'autofocus' => false]) ?>

                            <?= $form->field($model, 'email')->textInput(['maxlength' => 255, 'autofocus' => false]) ?>

                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-md-<?= $col6 ?>">
                                    <?= $form->field($model, 'first_name')->textInput(['maxlength' => 124]) ?>
                                </div>
                                <div class="col-md-<?= $col6 ?>">
                                    <?= $form->field($model, 'last_name')->textInput(['maxlength' => 124]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-<?= $col3 ?>">
                                    <?= $form->field($model, 'gender')->dropDownList(\common\models\User::getGenderList()) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-<?= $col3 ?>">
                                    <?= $form->field($model, 'birth_day')->textInput(['maxlength' => 2]) ?>
                                </div>
                                <div class="col-md-<?= $col3 ?>">
                                    <?= $form->field($model, 'birth_month')->dropDownList(CoreHelper::getMonthsList()) ?>
                                </div>
                                <div class="col-md-<?= $col3 ?>">
                                    <?= $form->field($model, 'birth_year')->textInput(['maxlength' => 4]) ?>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-<?= $col6 ?>">
                                    <?= $form->field($model, 'phone')->textInput(['maxlength' => 24]) ?>
                                </div>
                            </div>

                            <?= $form->field($model, 'info')->textarea(['maxlength' => 255]) ?>

                        </div>
                    </div>

                    <?= Html::submitButton(Yii::t('core/auth', 'Save Profile'), ['class' => 'btn btn-primary']) ?>


                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
    </div>
</div>



<?php
$confRemovingAuthMessage = Yii::t('core/auth', 'Are you sure you want to unlink this authorization?');
$confRemovingAvatarMessage = Yii::t('core/auth', 'Are you sure you want to delete your profile picture?');
$js = <<<JS
confRemovingAuthMessage = "{$confRemovingAuthMessage}";
confRemovingAvatarMessage = "{$confRemovingAvatarMessage}";
JS;

$this->registerJs($js, yii\web\View::POS_READY);
?>
