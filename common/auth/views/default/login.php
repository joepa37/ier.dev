<?php

/**
 * @var $this yii\web\View
 * @var $model common\auth\models\forms\LoginForm
 */
use common\widgets\ActiveForm;
use common\helpers\Html;
use common\assets\core\CoreAsset;
use frontend\assets\FrontendAsset;
use common\assets\check\MagicCheckAsset;
use common\assets\particle\ParticleAssset;

FrontendAsset::register($this);
MagicCheckAsset::register($this);
ParticleAssset::register($this);

$coreBaseUrl = $this->assetBundles[CoreAsset::class]->baseUrl;
$this->title = Yii::t('core/auth', 'Authorization');
?>

<div id="container" class="cls-container">
    <div id="particle-div" class="particle-div">
        <canvas id="particle-canvas"></canvas>
    </div>
    <div class="cls-content">
        <div id="main-panel" class="cls-content-sm panel">
            <div class="panel-alert"></div>
            <div class="panel-body">
                <div class="cls-language-selector">
                    <ul class="nav">
                        <li class="dropdown">
                            <a id="demo-lang-switch" class="lang-selector dropdown-toggle" href="#" data-toggle="dropdown">
                                <span class="lang-selected">
                                    <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/united-kingdom.png'; ?>" alt="English">
                                </span>
                            </a>

                            <!--Language selector menu-->
                            <ul class="head-list dropdown-menu dropdown-menu-right">
                                <li>
                                    <!--English-->
                                    <a href="#" class="active disabled">
                                        <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/united-kingdom.png'; ?>" alt="English">
                                        <span class="lang-id">EN</span>
                                        <span class="lang-name">English</span>
                                    </a>
                                </li>
                                <li>
                                    <!--France-->
                                    <a href="#">
                                        <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/france.png'; ?>" alt="France">
                                        <span class="lang-id">FR</span>
                                        <span class="lang-name">Fran&ccedil;ais</span>
                                    </a>
                                </li>
                                <li>
                                    <!--Germany-->
                                    <a href="#">
                                        <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/germany.png'; ?>" alt="Germany">
                                        <span class="lang-id">DE</span>
                                        <span class="lang-name">Deutsch</span>
                                    </a>
                                </li>
                                <li>
                                    <!--Italy-->
                                    <a href="#">
                                        <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/italy.png'; ?>" alt="Italy">
                                        <span class="lang-id">IT</span>
                                        <span class="lang-name">Italiano</span>
                                    </a>
                                </li>
                                <li>
                                    <!--Spain-->
                                    <a href="#">
                                        <img class="lang-flag" src="<?=$coreBaseUrl . '/img/flags/spain.png'; ?>" alt="Spain">
                                        <span class="lang-id">ES</span>
                                        <span class="lang-name">Espa&ntilde;ol</span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
                <div class="cls-title mar-ver pad-btm">
                    <h3 class="h4 mar-no"><?= Yii::t('core/auth', 'Account Login')?></h3>
                    <p class="text-muted"><?= Yii::t('core/auth', 'Sign In to your account')?></p>
                </div>
                <?php
                    $form = ActiveForm::begin([
                        'id' => 'login-form',
                        'options' => ['autocomplete' => 'off'],
                        'validateOnBlur' => true,
                        'fieldConfig' => [
                            'template' => "{error}\n{input}",
                            'errorOptions' => [
                                'tag' => 'label',
                                //'class' => 'help-block help-block-error control-label text-semibold',
                            ],
                        ],
                    ]);
                    Html::addCssClass($form->options, 'has-feedback-show-error-first');
                ?>
                <div class="form-group has-feedback">
                    <?= $form->field($model, 'username',
                        [
                            'feedbackIcon' => [
                                'default' => 'user',
                                'success' => 'user',
                                'error' => 'user',
                            ],
                        ])->textInput(['autofocus' => true, 'placeholder' => $model->getAttributeLabel('username'), 'autocomplete' => 'off']) ?>
                </div>
                <div class="form-group has-feedback">
                    <?= $form->field($model, 'password',
                        [
                            'feedbackIcon' => [
                                'default' => 'lock',
                                'success' => 'lock',
                                'error' => 'lock',
                            ],
                        ])->passwordInput(['placeholder' => $model->getAttributeLabel('password'), 'autocomplete' => 'off']) ?>
                </div>
                <div class="checkbox pad-btm text-left">
                    <?= $form->field($model, 'rememberMe',
                        [
                            'errorOptions' => [
                                'tag' => 'span',
                                'class' => 'hide',
                            ],
                        ])->checkbox([
                        'class' => 'magic-checkbox',
                        'labelOptions' => [
                            'id' => 'loginform-label-rememberme',
                        ],
                    ]);
                    ?>
                </div>
                <button class="btn btn-primary btn-lg btn-block" type="submit">Sign In</button>
                <?php ActiveForm::end() ?>
            </div>
            <div class="cls-footer">
                <a href="pages-password-reminder.html" class="btn-link">Forgot password ?</a>
                <div class="media pad-top bord-top">
                    <div class="cls-developer pull-right">
                        <a href="#" class="hidden-xs pad-rgt btn-link">Developed by: José Peña</a>
                        <a href="#" class="pad-rgt"><i class="demo-psi-facebook icon-lg text-primary"></i></a>
                        <a href="#" class="pad-rgt"><i class="demo-psi-twitter icon-lg text-info"></i></a>
                        <a href="#" class="pad-rgt"><i class="demo-psi-google-plus icon-lg text-danger"></i></a>
                    </div>
                    <div class="media-body text-right">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php
$js = <<<JS
    $('#loginform-label-rememberme').attr('for', 'loginform-rememberme');
JS;
    $this->registerJs($js);
?>