<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace common\auth;

use Yii;
use yii\base\Exception;

/**
 * @author José Peña <joepa37@gmail.com>
 */
class AuthAction extends \yii\authclient\AuthAction
{

    /**
     * Runs the action.
     */
    public function run()
    {
        try {
            return parent::run();
        } catch (Exception $ex) {
            Yii::$app->session->setFlash('error', $ex->getMessage());
            //Yii::$app->session->setFlash('error', Yii::t('core/auth', "Authentication error occured."));

            return $this->redirectCancel();
        }
    }
}