<?php
return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'bootstrap' => ['comments', 'core'],
    'language' => 'es',
    'sourceLanguage' => 'en',
    'components' => [
        'core' => [
            'class' => 'common\Core',
            'languages' =>
                [
                    'es' => 'Español',
                    'en' => 'English',
                ]
        ],
        'settings' => [
            'class' => 'common\components\Settings'
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'class' => 'common\components\User',
            'on afterLogin' => function ($event) {
                \common\models\UserVisitLog::newVisitor($event->identity->id);
            }
        ],
    ],
    'modules' => [
        'comments' => [
            'class' => 'common\comments\Comments',
            'userModel' => 'common\models\User',
            'userAvatar' => function ($user_id) {
                $user = common\models\User::findIdentity((int)$user_id);
                if ($user instanceof common\models\User) {
                    return $user->getAvatar();
                }
                return false;
            }
        ],
        'gridview' => [
            'class' => '\kartik\grid\Module',
        ],
        'treemanager' =>  [
            'class' => '\kartik\tree\Module',
        ],
    ],
];
