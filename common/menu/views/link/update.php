<?php

use yii\helpers\Html;
use common\menu\models\search\SearchMenuLink;
use yii\helpers\StringHelper;

/* @var $this yii\web\View */
/* @var $model common\models\MenuLink */

$formName = StringHelper::basename(SearchMenuLink::className());

$this->title = Yii::t('core/menu', 'Update Menu Link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/menu', 'Menus'), 'url' => ['/menu/default/index']];
$this->params['breadcrumbs'][] = ['label' => $model->menu->title, 'url' => ['/menu/default/index', "{$formName}[menu_id]" => $model->menu_id]];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="menu-link-update">
    <?= $this->render('_form', compact('model')) ?>
</div>
