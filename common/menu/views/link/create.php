<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\MenuLink */

$this->title = Yii::t('core/menu', 'Create Menu Link');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/menu', 'Menus'), 'url' => ['/menu/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="menu-link-create">
    <?= $this->render('_form', compact('model')) ?>
</div>