<?php

use common\grid\GridView;
use common\helpers\Html;
use common\models\Menu;
use common\models\User;
use common\menu\assets\MenuAsset;
use yii\bootstrap\Alert;
use yii\widgets\Breadcrumbs;

/* @var $this yii\web\View */
/* @var $searchModel common\menu\models\search\SearchMenu */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/menu', 'Menus');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core/menu', 'Add New Menu'), ['/menu/default/create'], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core/menu', 'Add New Link'), ['/menu/link/create'], ['class' => 'btn btn-sm btn-primary'])
];
$this->params['alerts'] = [
    Alert::widget([
        'options' => ['class' => 'alert-primary menu-link-alert'],
        'body' => '<span class="glyphicon glyphicon-refresh glyphicon-spin"></span>',
    ]),
    Alert::widget([
        'options' => ['class' => 'alert-danger menu-link-alert'],
        'body' => Yii::t('core/menu', 'An error occurred during saving menu!'),
    ]),
    Alert::widget([
        'options' => ['class' => 'alert-info menu-link-alert'],
        'body' => Yii::t('core/menu', 'The changes have been saved.'),
    ])
];

MenuAsset::register($this);
?>
<div class="menu-index">
    <div class="row">
        <div class="col-sm-4">
            <div class="box box-primary">
                <div class="panel-body">
                    <?=
                    GridView::widget([
                        'id' => 'menu-grid',
                        'dataProvider' => $dataProvider,
                        'layout' => '{items}',
                        'columns' => [
                            [
                                'class' => 'common\grid\columns\TitleActionColumn',
                                'controller' => '/menu/default',
                                'buttonsTemplate' => '{update} {delete}',
                                'title' => function (Menu $model) {
                                    if (User::hasPermission('viewMenuLinks')) {
                                        return Html::a($model->title, ['/menu/default/index', 'SearchMenuLink[menu_id]' => $model->id], ['data-pjax' => 0]);
                                    } else {
                                        return Html::a($model->title, ['/menu/default/view', 'id' => $model->id], ['data-pjax' => 0]);
                                    }
                                },
                            ],
                        ],
                    ])
                    ?>
                </div>
            </div>
        </div>

        <div class="col-sm-8">
            <div class="box box-success">
                <div class="panel-body">
                    <div class="sortable-container menu-itemes">
                        <?=
                        $this->render('links', [
                            'searchLinkModel' => $searchLinkModel,
                            'searchParams' => ['parent_id' => ''],
                        ]);
                        ?>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>


