<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\translation\models\MessageSource */

$this->title = Yii::t('core/translation', 'Create Message Source');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/translation', 'Message Translation'), 'url' => ['/translation/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="message-source-create">
    <?= $this->render('_form', compact('model')) ?>
</div>