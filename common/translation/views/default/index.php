<?php

use common\helpers\Html;
use common\models\User;
use common\widgets\ActiveForm;

/* @var $this common\web\View */

$this->title = Yii::t('core/translation', 'Message Translation');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core/translation', 'Add New Source Message'), ['/translation/source/create'], ['class' => 'btn btn-sm btn-primary'])
];
?>
    <div class="message-index">

        <div class="row">
            <div class="col-md-4">
                <div class="box box-primary">
                    <div class="panel-body translation">

                        <ul class="list-group">
                            <?php foreach ($categories as $category => $count) : ?>
                                <li>
                                    <h4>
                                        <b>[<?= strtoupper($category) ?>]</b>
                                        <?= Yii::t('core/translation', '{n, plural, =1{1 message} other{# messages}}', ['n' => $count]) ?>
                                    </h4>

                                    <?php foreach ($languages as $language => $languageLabel) : ?>
                                        <?php $link = ['/translation/default/index', 'category' => $category, 'translation' => $language] ?>
                                        <?php $options = (($currentLanguage == $language) && ($currentCategory == $category)) ? ['class' => 'active'] : [] ?>
                                        <?= Html::a("<span class='label label-default'>$languageLabel</span>", $link, $options) ?>
                                    <?php endforeach; ?>

                                </li>
                            <?php endforeach; ?>
                        </ul>

                    </div>
                </div>
            </div>

            <div class="col-md-8">
                <div class="box box-success">
                    <div class="panel-body">

                        <?php if (!$currentLanguage || !$currentCategory): ?>
                            <h4>
                                <?= Yii::t('core/translation', 'Please, select message group and language to view translations...') ?>
                            </h4>
                        <?php else: ?>

                            <?php $form = ActiveForm::begin() ?>

                            <?php if (User::hasPermission('updateSourceMessages')): ?>
                                <?= Html::submitButton(Yii::t('core', 'Save All'), ['class' => 'btn btn-sm btn-primary']) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/translation'], ['class' => 'btn btn-sm btn-default']) ?>
                                <div class="divider"></div>
                            <?php endif; ?>

                            <?php foreach ($messages as $index => $message) : ?>
                                <?php
                                $links = '';
                                if (User::hasPermission('updateSourceMessages') && (!$message->source->immutable || User::hasPermission('updateImmutableSourceMessages'))) {
                                    $links .= ' ' . Html::a('<span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>', ['/translation/source/update', 'id' => $message->source_id]);
                                }
                                ?>

                                <?= $form->field($message, "[$index]translation")->label($message->source->message . $links) ?>

                            <?php endforeach; ?>

                            <?php if (User::hasPermission('updateSourceMessages')): ?>
                                <div class="divider"></div>
                                <?= Html::submitButton(Yii::t('core', 'Save All'), ['class' => 'btn btn-sm btn-primary']) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/translation'], ['class' => 'btn btn-sm btn-default']) ?>
                            <?php endif; ?>

                            <?php ActiveForm::end() ?>

                        <?php endif; ?>

                    </div>
                </div>
            </div>
        </div>

    </div>

<?php
$this->registerCss("
    .translation li{
        display: block;
    }

    .translation li a:hover{
        text-decoration:none;
    }

    .translation li a:hover .label-default, .translation li a.active .label-default {
        background-color: #3c8dbc;
        border-color: #367fa9;
        color: white;
    }
    
    .translation li a {
        display: inline-block;
        padding-bottom: 4px;
    }
");