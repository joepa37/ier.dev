<?php

use common\helpers\Html;
use common\comments\widgets\Comments;
use common\post\models\Post;

/* @var $this yii\web\View */
/* @var $model common\models\Post */

$this->title = $model->titleTranslation;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/post/default/create'], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core', 'Edit'), ['/post/default/update', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary']),
    Html::a(Yii::t('core', 'Delete'), ['/post/default/delete', 'id' => $model->id], [
        'class' => 'btn btn-sm btn-warning pull-right',
        'data' => [
            'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
            'method' => 'post',
        ],
    ])
];
?>
<div class="post-view">

    <div class="panel panel-default">
        <div class="panel-body">
            <h2><?= $model->titleTranslation ?></h2>
            <?= $model->getThumbnail(['class' => 'thumbnail pull-left', 'style' => 'width: 240px; margin:0 7px 7px 0;']) ?>
            <?= $model->contentTranslation ?>
        </div>
    </div>

    <?php if ($model->comment_status == Post::COMMENT_STATUS_OPEN): ?>
        <?php echo Comments::widget(['model' => Post::className(), 'model_id' => $model->id]); ?>
    <?php endif; ?>


</div>
