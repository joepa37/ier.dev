<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\post\models\Tag */

$this->title = Yii::t('core/post', 'Create Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['/post/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Tags'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Create');
?>

<div class="post-tag-create">
    <?= $this->render('_form', compact('model')) ?>
</div>