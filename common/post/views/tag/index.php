<?php

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;
use common\post\models\Tag;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\post\models\search\TagSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/media', 'Tags');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['/post/default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/post/tag/create'], ['class' => 'btn btn-sm btn-primary'])
];
?>
<div class="post-tag-index">

    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'post-tag-grid-pjax']) ?>
                </div>
            </div>

            <?php Pjax::begin(['id' => 'post-tag-grid-pjax']) ?>

            <?= GridView::widget([
                'id' => 'post-tag-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'post-tag-grid',
                    'actions' => [Url::to(['bulk-delete']) => Yii::t('core', 'Delete')]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/post/tag',
                        'title' => function (Tag $model) {
                            return Html::a($model->titleTranslation, ['/post/tag/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],
                    'slug',
                ],
            ]); ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>