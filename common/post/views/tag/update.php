<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\post\models\Tag */

$this->title = Yii::t('core/media', 'Update Tag');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['/post/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Tags'), 'url' => ['/post/tag/index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="post-tag-update">
    <?= $this->render('_form', compact('model')) ?>
</div>