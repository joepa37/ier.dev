<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\post\models\Category */

$this->title = Yii::t('core/media', 'Update Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['/post/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Categories'), 'url' => ['/post/category/index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="post-category-update">
    <?= $this->render('_form', compact('model')) ?>
</div>