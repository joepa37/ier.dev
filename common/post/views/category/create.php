<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\post\models\Category */

$this->title = Yii::t('core/media', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Posts'), 'url' => ['/post/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/post', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Create');
?>

<div class="post-category-create">
    <?= $this->render('_form', compact('model')) ?>
</div>