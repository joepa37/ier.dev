<?php

use common\helpers\Html;
use yii\helpers\HtmlPurifier;
use common\comments\Comments;
use yii\timeago\TimeAgo;

/* @var $this yii\web\View */
?>

<div class="box box-primary">
    <div class="box-header with-border">
        <i class="fa fa-rss-square"></i>
        <h3 class="box-title"><?= Yii::t('core/post', 'Posts Activity') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <?php if (count($recentPosts)): ?>
        <ul class="products-list product-list-in-box">
            <?php foreach ($recentPosts as $post) : ?>
                <li class="item">
                    <a style="float: right; margin-right: 15px;">
                        <span><?= HtmlPurifier::process($post->author->fullName); ?></span>
                        <span style="color: #999; margin-left: 5px; margin-right: 5px;">  •  </span>
                            <span style="color: #999;">
                                <?php if(date('Ymd') == date('Ymd', $post->created_at)) : ?>
                                    <?= Comments::t('comments', 'Shared'); ?>
                                    <?= TimeAgo::widget(['timestamp' => $post->created_at]); ?>
                                <?php else : ?>
                                    <?= "{$post->publishedDate}"; ?>
                                <?php endif; ?>
                            </span>
                    </a>
                    <div class="product-img">
                        <?php
                        $bundle = \backend\assets\BackendAsset::register($this);
                        $noImage = $bundle->baseUrl . '/img/No image.png';
                        $options = [
                            'style' => 'height: auto; width: 80px; margin-right: 10px;',
                            'alt' => 'Post Thumbnail',
                        ];
                        if (!empty($post->thumbnail)) {
                            echo Html::a(\common\helpers\Html::img($post->thumbnail, $options), ['/post/default/view', 'id' => $post->id]);
                        }else{
                            echo Html::a(\common\helpers\Html::img($noImage, $options), ['/post/default/view', 'id' => $post->id]);
                        }
                        ?>
                    </div>
                    <div class="product-info">
                        <?= Html::a(HtmlPurifier::process($post->titleTranslation),
                            ['/post/default/view', 'id' => $post->id],
                            ['class' => 'product-title']
                        );
                        ?>
                            <span class="product-description" style="white-space: inherit; color: #444;">
                              <?= HtmlPurifier::process(mb_substr(strip_tags($post->contentTranslation), 0, 220, "UTF-8")); ?>
                              <?= (strlen(strip_tags($post->contentTranslation)) > 220) ? '...' : '' ?>
                            </span>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="box-footer text-center">
        <a class="uppercase">
            <?php $list = [] ?>
            <?php foreach ($posts as $post) : ?>
                <?php $list[] = Html::a("<b>{$post['count']}</b> {$post['label']}", $post['url']); ?>
            <?php endforeach; ?>
            <?= implode('<span style="color: #999; margin-left: 5px; margin-right: 5px;"> | </span>', $list) ?>
        </a>
        <?php else: ?>
            <h4><em><?= Yii::t('core/post', 'No posts found.') ?></em></h4>
        <?php endif; ?>
    </div>
</div>