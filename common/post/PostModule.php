<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace common\post;

use Yii;

/**
 * Post Module For IER
 *
 * @author José Peña <joepa37@gmail.com>
 */
class PostModule extends \yii\base\Module
{
    /**
     * Version number of the module.
     */
    const VERSION = '0.1.0';

    /**
     * Table aliases
     *
     * @var string
     */
    public $post_table          = '{{%post}}';
    public $controllerNamespace = 'common\post\controllers';
    public $viewList;
    public $layoutList;

    /**
     * Size of thumbnail image of the post.
     *
     * Expected values: 'original' or sizes from common\media\MediaModule::$thumbs,
     * by default there are: 'small', 'medium', 'large'
     *
     * @var string
     */
    public $thumbnailSize =  'medium';

    /**
     * Default views and layouts
     * Add more views and layouts in your main config file by calling the module
     *
     *   Example:
     *
     *   'post' => [
     *       'class' => 'common\post\PostModule',
     *       'viewList' => [
     *           'post' => 'View Label 1',
     *           'post_test' => 'View Label 2',
     *       ],
     *       'layoutList' => [
     *           'main' => 'Layout Label 1',
     *           'dark_layout' => 'Layout Label 2',
     *       ],
     *   ],
     */
    public function init()
    {
        if(in_array($this->thumbnailSize, [])){
            $this->thumbnailSize = 'medium';
        }

        if (empty($this->viewList)) {
            $this->viewList = [
                'post' => Yii::t('core', 'Post view')
            ];
        }

        if (empty($this->layoutList)) {
            $this->layoutList = [
                'main' => Yii::t('core', 'Main layout')
            ];
        }

        parent::init();
    }
}