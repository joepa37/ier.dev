<?php

namespace common\post\controllers;

use common\controllers\admin\BaseController;

/**
 * TagController implements the CRUD actions for common\post\models\Tag model.
 */
class TagController extends BaseController
{
    public $modelClass = 'common\post\models\Tag';
    public $modelSearchClass = 'common\post\models\search\TagSearch';
    public $disabledActions = ['view', 'bulk-activate', 'bulk-deactivate'];

    protected function getRedirectPage($action, $model = null)
    {
        switch ($action) {
            case 'update':
                return ['update', 'id' => $model->id];
                break;
            case 'create':
                return ['update', 'id' => $model->id];
                break;
            default:
                return parent::getRedirectPage($action, $model);
        }
    }
}