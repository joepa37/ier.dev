<?php
/* @var $this yii\web\View */
?>

<div class="box box-solid" style="border-radius: 6px;">

    <div class="box-body" style="padding: 0">
        <?php
            $bundle = \backend\assets\BackendAsset::register($this);
            $logo = $bundle->baseUrl . '/img/IER School 2.png';
            $options = [
                'class' => 'img-responsive pad',
                'style' => 'padding: 0;',
                'alt' => 'IER School'
            ];
            echo \common\helpers\Html::img($logo, $options);
        ?>
    </div>
</div>