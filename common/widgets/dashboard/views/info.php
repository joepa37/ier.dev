<?php
use common\Core;

/* @var $this yii\web\View */
?>


<div class="box box-solid bg-light-blue-gradient">
    <div class="box-header">
        <div class="pull-right box-tools">
            <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse" data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                <i class="fa fa-minus"></i></button>
        </div>
        <i class="fa fa-info"></i>

        <h3 class="box-title">
            <?= Yii::t('core', 'System Info') ?>
        </h3>
    </div>
    <div class="box-body">
        <b><?= Yii::t('core', 'IER School Version') ?>:</b> <?= Yii::$app->params['version']; ?><br/>
        <b><?= Yii::t('core', 'Core Version') ?>:</b> <?= Core::getVersion(); ?><br/>
        <b><?= Yii::t('core', 'Yii Framework Version') ?>:</b> <?= Yii::getVersion(); ?><br/>
        <b><?= Yii::t('core', 'PHP Version') ?>:</b> <?= phpversion(); ?><br/>
    </div>
</div>