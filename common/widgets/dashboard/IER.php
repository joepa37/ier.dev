<?php

namespace common\widgets\dashboard;

use common\widgets\DashboardWidget;

class IER extends DashboardWidget
{
    public function run()
    {
        return $this->render('ier',
            [
                'height' => $this->height,
                'width' => $this->width,
                'position' => $this->position,
            ]);
    }
}