<?php

use common\widgets\assets\LanguageSelectorAsset;
use yii\helpers\ArrayHelper;

LanguageSelectorAsset::register($this);
?>

<ul class="nav navbar-nav">
    <li class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo Yii::t('core','Language');?> <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
            <?php foreach ($languages as $key => $lang) : ?>
                <?php if (Yii::$app->core->getDisplayLanguageShortcode($language) == $key) : ?>
                    <li role="language" class="active">
                        <a><?= ($display == 'code') ? $key : $lang ?></a>
                    </li>
                <?php else: ?>
                    <?php $link = Yii::$app->urlManager->createUrl(ArrayHelper::merge($params, [$url, 'language' => $key])); ?>
                    <li role="language">
                        <a href="<?= $link ?>"><?= ($display == 'code') ? $key : $lang ?></a>
                    </li>
                <?php endif; ?>
            <?php endforeach; ?>
        </ul>
    </li>
</ul>
