<?php

namespace common\grid;

class GridView extends \kartik\grid\GridView
{
    public $bulkActions;
    public $bulkActionOptions = [];
    public $filterPosition = self::FILTER_POS_HEADER;
    public $pager = [
        'options' => ['class' => 'pagination pagination-sm'],
        'hideOnSinglePage' => true,
        'firstPageLabel' => '<<',
        'prevPageLabel' => '<',
        'nextPageLabel' => '>',
        'lastPageLabel' => '>>',
    ];
    public $tableOptions = ['class' => 'table table-striped'];
    public $layout = '{items}<div class="row"><div class="col-sm-3 m-tb-20">{bulkActions}</div><div class="col-sm-6 text-center">{pager}</div><div class="col-sm-3 text-right m-tb-20">{summary}</div></div>';

    public function init()
    {
        parent::init();
        $this->registerCSS();
    }

    public function renderSection($name)
    {
        switch ($name) {
            case '{bulkActions}':
                return $this->renderBulkActions();
            default:
                return parent::renderSection($name);
        }
    }

    public function renderBulkActions()
    {
        if (!$this->bulkActions) {
            $this->bulkActions = GridBulkActions::widget($this->bulkActionOptions);
        }
        return $this->bulkActions;
    }

    public function registerCSS(){
        $css = <<< CSS
            .pagination {
                margin-top: -3px;
                margin-bottom: 0;
            }
            .col-sm-3.m-tb-20 .form-inline{
                margin-top: -3px;
            }
            .form-inline.pull-right, .col-sm-6 .btn-group{
                margin-top: -5px;
                margin-bottom: 10px;
            }
CSS;
        $this->getView()->registerCss($css);
    }
}
