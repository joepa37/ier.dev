<?php

namespace common\admin;

use Yii;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'backend\controllers';

    public function init()
    {
        parent::init();
    }
}
