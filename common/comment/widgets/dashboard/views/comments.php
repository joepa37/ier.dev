<?php


use common\comments\Comments;
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
use yii\timeago\TimeAgo;

/* @var $this yii\web\View */
?>

<div class="box box-info">
    <div class="box-header with-border">
        <i class="fa fa-comments"></i>
        <h3 class="box-title"><?= Yii::t('core/comment', 'Comments Activity') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body">
        <?php if (count($recentComments)): ?>
        <ul class="products-list product-list-in-box">
            <?php foreach ($recentComments as $comment) : ?>
                <li class="item">
                    <a style="float: right;  margin-right: 15px;">
                        <span style="color: #999;">
                            <?php if(date('Ymd') == date('Ymd', $comment->created_at)) : ?>
                                <?= Comments::t('comments', 'Shared'); ?>
                                <?= TimeAgo::widget(['timestamp' => $comment->created_at]); ?>
                            <?php else : ?>
                                <?= "{$comment->createdDateTime}"; ?>
                            <?php endif; ?>
                        </span>
                    </a>
                    <div class="user-block">
                        <?php
                            $bundle = \backend\assets\BackendAsset::register($this);
                            $defaultAvatar = $bundle->baseUrl . '/img/No profile picture.png';
                            $options = [
                                'class' => 'img-circle img-bordered-sm'
                            ];
                            $avatar = ($userAvatar = $comment->user->getAvatar()) ? $userAvatar : $defaultAvatar;
                            echo Html::a(\common\helpers\Html::img($avatar, $options), ['/user/default/update', 'id' => $comment->user_id]);

                            /*$avatar = Comments::getInstance()->renderUserAvatar($comment->user_id);
                            $options = [
                                'class' => 'img-circle img-bordered-sm'
                            ];
                            echo Html::a(\common\helpers\Html::img($avatar, $options), ['/user/default/update', 'id' => $comment->user_id]);*/
                        ?>
                        <div class="product-info" style="margin-left: 50px;">
                            <?= Html::a(HtmlPurifier::process(Html::encode($comment->user->fullName)),
                                ['/user/default/update', 'id' => $comment->user_id],
                                ['class' => 'product-title']
                            );
                            ?>
                            <span class="product-description" style="white-space: inherit; color: #444;">
                                <?= $comment->getShortContent(104) ?>
                                <?= Html::a(Yii::t('core/comment', 'Read more'), $comment->url, ['class' => 'pull-right']);
                                ?>
                            </span>
                        </div>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="box-footer text-center">
        <a class="uppercase">
            <?php $list = [] ?>
            <?php foreach ($comments as $comment) : ?>
                <?php $list[] = Html::a("<b>{$comment['count']}</b> {$comment['label']}", $comment['url']); ?>
            <?php endforeach; ?>
            <?= implode('<span style="color: #999; margin-left: 5px; margin-right: 5px;"> | </span>', $list) ?>
        </a>
        <?php else: ?>
            <h4><em><?= Yii::t('core/comment', 'No comments found.') ?></em></h4>
        <?php endif; ?>
    </div>
</div>