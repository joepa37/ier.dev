<?php

namespace common\comment\widgets;

use common\comment\models\search\CommentSearch;
use common\comments\models\Comment;
use common\models\User;
use common\post\models\Post;
use common\widgets\DashboardWidget;
use Yii;

class RecentComments extends DashboardWidget
{

    /**
     * Most recent comments limit
     */
    public $recentLimit = 5;
    
    public $layout = 'layout';
    public $commentTemplate = 'comment';

    public function run()
    {
        $recentComments = Comment::find()
                ->active()
                ->filterModel(Post::className())
                ->orderBy(['id' => SORT_DESC])
                ->limit($this->recentLimit)
                ->all();

        return $this->render($this->layout, [
            'height' => $this->height,
            'width' => $this->width,
            'recentComments' => $recentComments,
            'commentTemplate' => $this->commentTemplate,
        ]);
    }

}
