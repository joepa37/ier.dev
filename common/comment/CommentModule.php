<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace common\comment;

/**
 * Comments Module For Core
 *
 * @author José Peña <joepa37@gmail.com>
 */
class CommentModule extends \yii\base\Module
{
    /**
     * Version number of the module.
     */
    const VERSION = '0.1.0';

    public $controllerNamespace = 'common\comment\controllers';

}