<?php

use common\grid\columns\DateRangePicker\DateRangePicker;
use common\comments\Comments;
use common\comments\models\Comment;
use common\grid\GridPageSize;
use common\grid\GridQuickLinks;
use common\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel common\comment\models\search\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Comments::t('comments', 'Comments');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <div class="box box-primary">
        <div class="panel-body">

            <?php
            Pjax::begin([
                'id' => 'comment-grid-pjax',
            ])
            ?>

            <div class="row">
                <div class="col-sm-6">
                    <?php
                    echo GridQuickLinks::widget([
                        'model' => Comment::className(),
                        'searchModel' => $searchModel,
                        'options' => [
                            ['label' => Yii::t('core', 'All'), 'filterWhere' => []],
                            ['label' => Yii::t('core', 'Approved'), 'filterWhere' => ['status' => Comment::STATUS_APPROVED]],
                            ['label' => Yii::t('core', 'Pending'), 'filterWhere' => ['status' => Comment::STATUS_PENDING]],
                            ['label' => Yii::t('core', 'Spam'), 'filterWhere' => ['status' => Comment::STATUS_SPAM]],
                            ['label' => Yii::t('core', 'Trash'), 'filterWhere' => ['status' => Comment::STATUS_TRASH]],
                        ]
                    ])
                    ?>
                </div>

                <div class="col-sm-6 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'comment-grid-pjax']) ?>
                </div>
            </div>



            <?=
            GridView::widget([
                'id' => 'comment-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'comment-grid',
                    'actions' => [
                        Url::to(['bulk-activate']) => Comments::t('comments', 'Approve'),
                        Url::to(['bulk-deactivate']) => Comments::t('comments', 'Unapprove'),
                        Url::to(['bulk-spam']) => Comments::t('comments', 'Mark as Spam'),
                        Url::to(['bulk-trash']) => Comments::t('comments', 'Move to Trash'),
                        Url::to(['bulk-delete']) => Yii::t('core', 'Delete'),
                    ]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/comment/default',
                        'attribute' => 'content',
                        'title' => function (Comment $model) {
                            return Html::a(mb_substr($model->content, 0, 32) . '...',
                                ['/comment/default/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],
                    [
                        'label' => Yii::t('core', 'User'),
                        'value' => function (Comment $model) {
                            return $model->getAuthor();
                        },
                        'options' => ['style' => 'width:150px'],
                    ],
                    [
                        'attribute' => 'model',
                        'value' => function (Comment $model) {
                            return $model->model . (($model->model_id) ? ' [' . $model->model_id . ']'
                                : '');
                        },
                        'options' => ['style' => 'width:120px'],
                    ],
                    // 'email:email',
                    // 'parent_id',
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'status',
                        'optionsArray' => Comment::getStatusOptionsList(),
                        'options' => ['style' => 'width:100px'],
                    ],
                    [
                        'attribute' => 'created_at',
                        'value' => function ($model) {
                            return $model->createdDatetime;
                        },
                        'options' => ['style' => 'width:150px'],
                    ],
                    // 'updated_at',
                    [
                        'attribute' => 'user_ip',
                        'options' => ['style' => 'width:100px'],
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php
DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'created_at',
])
?>