<?php

use common\comments\Comments;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\comments\models\Comment */

$this->title = Yii::t('core', 'Update "{item}"', ['item' => Comments::t('comments', 'Comment')]);
$this->params['breadcrumbs'][] = ['label' => Comments::t('comments', 'Comments'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="comment-update">
    <?= $this->render('_form', compact('model')) ?>
</div>