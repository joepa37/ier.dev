<?php

namespace common\web;

use Yii;
use yii\base\Action;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

class DashboardAction extends Action
{
    public $widgets;
    public $layout;

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws NotFoundHttpException if the view file cannot be found
     */
    public function run()
    {
        $this->controller->getView()->title = Yii::t('core', 'Dashboard');
        $this->controller->getView()->params['breadcrumbs'][] = $this->controller->getView()->title;
        $this->controller->getView()->params['subtitle'] =  Yii::t('core', 'Control panel');

        if (!is_array($this->widgets)) {
            throw new NotFoundHttpException(Yii::t('core', 'Invalid settings for dashboard widgets.'));
        }

        $controllerLayout = null;
        if ($this->layout !== null) {
            $controllerLayout = $this->controller->layout;
            $this->controller->layout = $this->layout;
        }

        try {
            $output = $this->render();

            if ($controllerLayout) {
                $this->controller->layout = $controllerLayout;
            }
        } catch (InvalidParamException $e) {

            if ($controllerLayout) {
                $this->controller->layout = $controllerLayout;
            }

            if (YII_DEBUG) {
                throw new NotFoundHttpException($e->getMessage());
            } else {
                throw new NotFoundHttpException(
                    Yii::t('yii', 'The requested view was not found.')
                );
            }
        }

        return $output;
    }

    /**
     * Renders a view
     *
     * @return string result of the rendering
     */
    protected function render()
    {
        $content = '<div class="row">';
        $leftContent = '<section class="col-lg-8 connectedSortable">';
        $rightContent = '<section class="col-lg-4 connectedSortable">';

        foreach ($this->widgets as $widget) {
            if (is_string($widget)) {

                $leftContent .= $widget::widget();

            } elseif (is_array($widget)) {

                if (!isset($widget['class'])) {
                    throw new NotFoundHttpException(Yii::t('core', 'Invalid settings for dashboard widgets.'));
                }

                $class = $widget['class'];
                $settings = $widget;
                unset($settings['class']);
                if($widget['position'] == 'left'){
                    $leftContent .= $class::widget($settings);
                }else{
                    $rightContent .= $class::widget($settings);
                }

            } else {
                throw new NotFoundHttpException(Yii::t('core', 'Invalid settings for dashboard widgets.'));
            }
        }

        $content .= $leftContent . '</section>' . $rightContent . '</section></div>';

        //$this->registerDraggable();
        return $this->controller->renderContent($content);

    }

    /*public function registerDraggable(){
        $js = <<<JS
    $(function () {
          "use strict";
          $(".connectedSortable").sortable({
            placeholder: "sort-highlight",
            connectWith: ".connectedSortable",
            handle: ".box-header, .nav-tabs",
            forcePlaceholderSize: true,
            zIndex: 999999
          });
          $(".connectedSortable .box-header, .connectedSortable .nav-tabs-custom").css("cursor", "move");
        });
JS;
        $this->controller->getView()->registerJs($js);
    }*/
}