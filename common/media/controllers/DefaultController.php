<?php

namespace common\media\controllers;

use common\controllers\admin\BaseController;
use common\media\models\Media;

class DefaultController extends BaseController
{

    public $disabledActions = ['view', 'create', 'update', 'delete', 'toggle-attribute',
        'bulk-activate', 'bulk-deactivate', 'bulk-delete', 'grid-sort', 'grid-page-size'];

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionSettings()
    {
        return $this->render('settings');
    }

    public function actionComment($id)
    {
        $tableName = Media::tableName();
        $model = Media::findOne(["{$tableName}.id" => $id]);
        return $this->render('comment', compact('model'));
    }

}