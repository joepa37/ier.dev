<?php

use common\helpers\Html;
use common\media\assets\MediaAsset;
use common\media\models\Album;
use common\models\User;
use yii\grid\GridViewAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel common\media\models\Media */
/* @var $dataProvider yii\data\ActiveDataProvider */


//\common\assets\AdminLte::register($this);
$this->params['moduleBundle'] = MediaAsset::register($this);

GridViewAsset::register($this);
?>

<div class="row">
    <div class="col-sm-12">
        <div class="box box-primary" style="margin-bottom: 10px;">
            <div style="padding: 5px; height:50px;" class="panel-body">

                <?php
                $form = ActiveForm::begin([
                    'id' => 'gallery',
                    'action' => Url::to(($mode == 'modal') ? ['/media/manage/index'] : ['/media/default/index']),
                    'method' => 'get',
                    'class' => 'gridview-filter-form',
                    'fieldConfig' => ['template' => "{input}\n{hint}\n{error}"],
                ]);
                ?>
                <table id="gallery-grid-filters" class="table table-striped filters">
                    <thead>
                    <tr id="gallery-grid-filters" class="filters">
                        <td style="width: 30%;">
                            <?= $form->field($searchModel, 'album_id')->widget(\kartik\widgets\Select2::classname(), [
                                'data' => ArrayHelper::merge(['' => Yii::t('core/media', 'All Media Items')], Album::getAlbums(true, true)),
                                'options' => ['placeholder' => Yii::t('core/school', 'Select an album').'...'],
                                'pluginOptions' => [
                                    'allowClear' => true
                                ],
                                'addon' => [
                                    'prepend' => [
                                        'content' => Html::icon('book')
                                    ],
                                ]
                            ]);?>
                        </td>
                        <td style="width: auto;">
                            <?= $form->field($searchModel, 'title')->textInput(['placeholder' => $searchModel->attributeLabels()['title']]) ?>
                        </td>
                        <td style="width: auto;">
                            <?= $form->field($searchModel, 'created_at')->textInput(['placeholder' => $searchModel->attributeLabels()['created_at']]) ?>
                        </td>
                        <?php if (User::hasPermission('uploadMedia')): ?>
                            <td style="width: 1%;">
                                <?= Html::a(Yii::t('core/media', 'Upload New File'), ($mode == 'modal') ? ['/media/manage/uploader', 'mode' => 'modal'] : ['/media/manage/uploader'], ['class' => 'btn btn-primary pull-right']) ?>
                            </td>
                        <?php endif; ?>
                    </tr>
                    </thead>
                </table>
                <?php ActiveForm::end(); ?>
            </div>
            <div class="divider"></div>
        </div>
    </div>
</div>

<div class="row <?= $mode ?>-media-frame">
    <div class="col-md-8" style="padding-right: 0;">
        <div class = "box box-success">
            <div class="panel-body">
                <div id="media" data-frame-mode="<?= $mode ?>" data-url-info="<?= Url::to(['/media/manage/info']) ?>">
                    <?= $this->render('list-view', compact('dataProvider')); ?>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">

        <div class="box box-success">
            <div class="panel-body media-details">
                <div class="dashboard">
                    <div class="row">
                        <div class="col-sm-12">
                            <h5><b><?= Yii::t('core/media', 'Media Details') ?>:</b></h5>
                            <div id="fileinfo">
                                <h6><?= Yii::t('core/media', 'Please, select file to view details.') ?></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php

//Init AJAX filter submit
$options = '{"filterUrl":"' . Url::to(['default/index']) . '","filterSelector":"#gallery-grid-filters input, #gallery-grid-filters select"}';
$this->registerJs("jQuery('#gallery').yiiGridView($options);");

?>

<?php
\common\grid\columns\DateRangePicker\DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'created_at',
])
?>