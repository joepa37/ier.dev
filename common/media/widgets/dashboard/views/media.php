<?php

use common\helpers\Html;
use yii\grid\GridViewAsset;
use common\media\assets\MediaAsset;

/* @var $this yii\web\View */
/* @var $item common\media\models\Media */

//\common\assets\AdminLte::register($this);
$this->params['moduleBundle'] = MediaAsset::register($this);

GridViewAsset::register($this);
?>

    <div class="box box-success">
        <div class="box-header with-border">
            <i class="fa fa-image"></i>
            <h3 class="box-title"><?= Yii::t('core/post', 'Media Activity') ?></h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body" style="margin-bottom: -10px;">
            <?php if (count($recent)): ?>
                <?= $this->render('@common/media/widgets/views/list-view', compact('dataProvider')); ?>
        </div>
        <div class="box-footer text-center">
            <a class="uppercase">
                <?= Html::a(Yii::t('core/media', 'View all media'), \yii\helpers\Url::toRoute('media/default/index')); ?>
            </a>
            <?php else: ?>
                <h4><em><?= Yii::t('core/post', 'No media found.') ?></em></h4>
            <?php endif; ?>
        </div>
    </div>