<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\media\models\Album */

$this->title = Yii::t('core', 'Create {item}', ['item' => Yii::t('core/media', 'Album')]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="album-create">
    <?= $this->render('_form', compact('model')) ?>
</div>