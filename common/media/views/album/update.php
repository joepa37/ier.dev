<?php

use common\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\media\models\Album */

$this->title = Yii::t('core', 'Update {item}', ['item' => Yii::t('core/media', 'Album')]);
$this->params['subtitle'] = '7mo grado';
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS])
];
?>
<div class="album-update">
    <?= $this->render('_form', compact('model')) ?>
</div>