<?php

use dosamigos\fileupload\FileUploadUI;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $searchModel common\media\models\Media */

$this->title = Yii::t('core/media', 'Upload New File');

if ($mode !== 'modal') {
    $this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['default/index']];
    $this->params['breadcrumbs'][] = $this->title;
}
?>

<div class="box box-primary" style="margin-bottom: 10px;">
    <div class="panel-body">
        <div id="uploadmanager">
            <p>
                <?= Html::a('← ' . Yii::t('core/media', 'Back to file manager'), ($mode == 'modal') ? ['manage/index', 'mode' => 'modal'] : ['default/index']) ?>
            </p>

            <?= FileUploadUI::widget([
                'model' => $model,
                'attribute' => 'file',
                'formView' => '@common/media/views/upload-widget/form',
                'uploadTemplateView' => '@common/media/views/upload-widget/upload',
                'downloadTemplateView' => '@common/media/views/upload-widget/download',
                'clientOptions' => [
                    'autoUpload' => Yii::$app->getModule('media')->autoUpload,
                ],
                'url' => ['upload'],
                'gallery' => false,
            ]) ?>

        </div>
    </div>
</div>