<div class="modal modal fade in" data-wysihtml5-dialog="insertImage" style="padding-right: 17px;"
     role="media-modal" tabindex="-1" style="z-index:9999999"
     data-frame-id="<?= $frameId ?>"
     data-frame-src="<?= $frameSrc ?>"
     data-btn-id="<?= $btnId ?>"
     data-input-id="<?= $inputId ?>"
     data-image-container="<?= isset($imageContainer) ? $imageContainer : '' ?>"
     data-paste-data="<?= isset($pasteData) ? $pasteData : '' ?>"
     data-thumb="<?= $thumb ?>">
    <div class="modal-dialog ">
        <div class="modal-content" style="border-radius: 4px;">
            <div class="modal-body" style="background-color: #ecf0f5; border-radius: 4px;"></div>
        </div>
    </div>
</div>