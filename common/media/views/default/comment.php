<?php

use common\comments\Comments;
use common\helpers\Html;
use yii\timeago\TimeAgo;
use common\comments\widgets\Comments as CommentsWidget;

/* @var $this yii\web\View */

$this->title = Yii::t('core/media', 'Comment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['subtitle'] = substr($model->title, 0, 70);
if(strlen($model->title) > 70){
    $this->params['subtitle'].= '...';
}
?>

<div class="box box-widget">
    <div class="box-header with-border">
        <div class="user-block">
            <img class="img-circle" src="<?= Comments::getInstance()->renderUserAvatar($model->author->id) ?>"  alt="User Image">
            <span class="username"><a><?= Html::encode($model->author->fullName); ?></a></span>
            <span class="description">
                <?php if(date('Ymd') == date('Ymd', $model->created_at)) : ?>
                    <?= Comments::t('comments', 'Shared'); ?>
                    <?= TimeAgo::widget(['timestamp' => $model->created_at]); ?>
                <?php else : ?>
                    <?= $model->createdDateTime; ?>
                <?php endif; ?>
            </span>
        </div>
        <div class="box-tools">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
        </div>
    </div>
        <?php if($model->isImage()) : ?>
        <div class="box-body" style="display: block;">
            <img class="img-responsive pad" src="<?= $model->getThumbUrl('original'); ?>" alt="<?= $model->alt ?>">
            <p style="margin: 0 10px 0;">
                <?php
                    if(!empty($model->description)){
                        echo $model->description;
                    }else{
                        echo Comments::t('core/media', 'No description set.');
                    }
                    ?>
            </p>
        </div>
        <?php else : ?>
            <div class="box-body" style="display: block; padding: 0;">
                <div class="attachment-block clearfix" style="margin-bottom: 0;">
                    <?php
                        $fa = $model->getFA_Array();
                        $span = '<span class="attachment-img" style="margin-left: 10px;">
                                    <i class="'.$fa['name'].'" style="color:'.$fa['color'].'; font-size: 7em;"></i>
                                </span>';
                        echo Html::a($span, \yii\helpers\Url::to($model->getThumbUrl('original')), ['target' => '_blank']);
                    ?>

                    <div class="attachment-pushed">
                        <h4 class="attachment-heading">
                            <?php
                                echo '<a target="_blank"  href='.$model->getThumbUrl('original').'>'.$model->title.'</a>';
                            ?>
                        </h4>

                        <div class="attachment-text">
                            <?php
                                if(!empty($model->description)){
                                    echo $model->description;
                                }else{
                                    echo Comments::t('core/media', 'No description set.');
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif; ?>
    <div class="box-footer">
        <?php echo CommentsWidget::widget(['model' => \common\media\models\Media::className(), 'model_id' => $model->id, 'is_box' => false]); ?>
    </div>
</div>