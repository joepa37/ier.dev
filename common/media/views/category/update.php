<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\media\models\Category */

$this->title = Yii::t('core/media', 'Update Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Categories'), 'url' => ['/media/category/index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>
<div class="media-category-update">
    <?= $this->render('_form', compact('model')) ?>
</div>