<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\media\models\Category */

$this->title = Yii::t('core/media', 'Create Category');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Media'), 'url' => ['/media/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Albums'), 'url' => ['/media/album/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/media', 'Categories'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core', 'Create');
?>

<div class="media-category-create">
    <?= $this->render('_form', compact('model')) ?>
</div>