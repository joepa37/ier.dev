<!-- The template to display files available for download -->
<script id="template-download" type="text/x-tmpl">
{% for (var i=0, file; file=o.files[i]; i++) { %}
    <tr class="template-download fade">
        <td>
            <span class="preview">
                {% if (file.thumbnailUrl) { %}
                    <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery><img src="{%=file.thumbnailUrl%}"></a>
                {% } else { %}
                    {% if (file.FA) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" data-gallery>
                            <span class="attachment-img" style="float: left;">
                                <i class="{%=file.FA['name']%}" style="color:{%=file.FA['color']%}; font-size: 7em;"></i>
                            </span>
                        </a>
                    {% } %}
                {% } %}
            </span>
        </td>
        <td>
            {% if (file.name) { %}
                <p class="name">
                    {% if (file.url) { %}
                        <a href="{%=file.url%}" title="{%=file.name%}" download="{%=file.name%}" {%=file.thumbnailUrl?'data-gallery':''%}>{%=file.name%}</a>
                    {% } else { %}
                        <span>{%=file.name%}</span>
                    {% } %}
                </p>
            {% } %}
            {% if (file.error) { %}
                <div><span class="label label-danger"><?= \Yii::t('core', 'Error') ?></span> {%=file.error%}</div>
            {% } %}
        </td>
        <td style="text-align: center;">
            <span class="size">{%=o.formatFileSize(file.size)%}</span>
        </td>
        <td style="text-align: center;">
            {% if (file.deleteUrl) { %}
                <button class="btn btn-default delete" data-type="{%=file.deleteType%}" data-url="{%=file.deleteUrl%}"{% if (file.deleteWithCredentials) { %} data-xhr-fields='{"withCredentials":true}'{% } %}>
                    <i class="glyphicon glyphicon-trash"></i>
                    <span><?= \Yii::t('core', 'Delete') ?></span>
                </button>
            {% } else { %}
                <button class="btn btn-default cancel">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span><?= \Yii::t('core', 'Cancel') ?></span>
                </button>
            {% } %}
        </td>
    </tr>
{% } %}




</script>
