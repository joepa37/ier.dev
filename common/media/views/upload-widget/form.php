<?php
/** @var \dosamigos\fileupload\FileUploadUI $this */

use yii\helpers\ArrayHelper;
use common\media\models\Album;
use common\widgets\ActiveForm;
use common\helpers\Html;

$context = $this->context;

$js = <<< JS
    $('.submit-button').click(function(){
        $('.upload').submit();
    });
    $('.reset-button').click(function(){
        $('.upload').reset();
    });
JS;
$this->registerJs($js);
?>

    <!-- The file upload form used as target for the file upload widget -->
    <?php
    $form = ActiveForm::begin([
        'id' => 'upload',
        'action' => $context->url,
        'method' => 'post',
        'options' => $context->options
    ]);
    ?>
    <!-- The fileupload-buttonbar contains buttons to add/delete files and start/cancel the upload -->
    <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <?= $form->field($context->model, 'set_album_id', ['showLabels' => false])->widget(\kartik\widgets\Select2::classname(), [
                'data' => ArrayHelper::merge(['' => Yii::t('core/media', 'All Media Items')], Album::getAlbums(true, true)),
                'options' => ['placeholder' => Yii::t('core/school', 'Select an album').'...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                'addon' => [
                    'prepend' => [
                        'content' => Html::icon('book')
                    ],
                    'append' => [
                        'content' => Html::icon('arrow-right')
                    ],
                ]
            ]); ?>
        </div>
        <div class="col-md-5 col-sm-12 col-xs-12">
            <div class="fileupload-buttonbar btn-group btn-group-justified" style="margin-bottom: 15px;">
                <!-- The fileinput-button span is used to style the file input field as button -->
                <span class="btn btn-success fileinput-button">
                    <i class="glyphicon glyphicon-plus"></i>
                    <span><?= Yii::t('core/media', 'Add files') ?>...</span>
                    <?= $context->model instanceof \yii\base\Model && $context->attribute !== null
                        ? Html::activeFileInput($context->model, $context->attribute, $context->fieldOptions)
                        : Html::fileInput($context->name, $context->value, $context->fieldOptions); ?>
                </span>
                <span class="btn btn-primary start submit-button">
                    <i class="glyphicon glyphicon-upload"></i>
                    <span><?= Yii::t('core/media', 'Start upload') ?></span>
                </span>
                <span class="btn btn-default cancel cancel-button">
                    <i class="glyphicon glyphicon-ban-circle"></i>
                    <span><?= Yii::t('core/media', 'Cancel upload') ?></span>
                </span>
            </div>
        </div>
        <div class="col-md-4 col-sm-12 col-xs-12">
            <!-- The global file processing state -->
            <span class="fileupload-process"></span>
            <!-- The global progress state -->
            <div class="fileupload-progress fade" style="margin-bottom: 15px;">
                <!-- The global progress bar -->
                <div class="progress active" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="margin-bottom: 0; height: 32px; border-radius: 3px;">
                    <div class="progress-bar progress-bar-primary" style="width:0%;"></div>
                </div>
                <!-- The extended global progress state -->
                <div class="progress-extended" style="text-align: center;">&nbsp;</div>
            </div>
        </div>
    </div>
    <!-- The table listing the files available for upload/download -->
    <table role="presentation" class="table table-striped">
        <tbody class="files"></tbody>
    </table>

<?php ActiveForm::end(); ?>