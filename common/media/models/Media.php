<?php

namespace common\media\models;

use omgdef\multilingual\MultilingualQuery;
use common\behaviors\MultilingualBehavior;
use common\media\MediaModule;
use common\models\OwnerAccess;
use common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use common\db\ActiveRecord;
use yii\helpers\Html;
use yii\helpers\Inflector;
use yii\imagine\Image as Imagine;
use yii\web\UploadedFile;

/**
 * This is the model class for table "media".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $filename
 * @property string $type
 * @property string $url
 * @property string $title
 * @property string $alt
 * @property integer $size
 * @property string $description
 * @property string $thumbs
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 */
class Media extends ActiveRecord implements OwnerAccess
{
    public $file;
    public $set_album_id;
    public static $imageFileTypes = ['image/gif', 'image/jpeg', 'image/png'];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%media}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['filename', 'type'], 'required'],
            [['alt', 'description', 'thumbs'], 'string'],
            [['created_by', 'updated_by', 'created_at', 'updated_at', 'size', 'album_id'], 'integer'],
            [['filename', 'type', 'title'], 'string', 'max' => 255],
            [['file'], 'file'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('core', 'ID'),
            'album_id' => Yii::t('core/media', 'Album'),
            'filename' => Yii::t('core/media', 'Filename'),
            'type' => Yii::t('core', 'Type'),
            'url' => Yii::t('core', 'URL'),
            'title' => Yii::t('core', 'Title'),
            'alt' => Yii::t('core/media', 'Alt Text'),
            'size' => Yii::t('core', 'Size'),
            'description' => Yii::t('core', 'Description'),
            'thumbs' => Yii::t('core/media', 'Thumbnails'),
            'created_at' => Yii::t('core', 'Uploaded'),
            'updated_at' => Yii::t('core', 'Updated'),
            'created_by' => Yii::t('core/media', 'Uploaded By'),
            'updated_by' => Yii::t('core/media', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
            'multilingual' => [
                'class' => MultilingualBehavior::className(),
                'langForeignKey' => 'media_id',
                'tableName' => "{{%media_lang}}",
                'attributes' => [
                    'title', 'description', 'alt',
                ]
            ],
        ];
    }

    /**
     * @inheritdoc
     * @return static|null ActiveRecord instance matching the condition, or `null` if nothing matches.
     */
    public static function findOne($condition)
    {
        return static::findByCondition($condition)->joinWith('translations')->one();
    }

    /**
     * @inheritdoc
     * @return static[] an array of ActiveRecord instances, or an empty array if nothing matches.
     */
    public static function findAll($condition)
    {
        return static::findByCondition($condition)->joinWith('translations')->all();
    }

    /**
     * Return created_by user instance
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * Save just uploaded file
     *
     * @param array $routes routes from module settings
     * @return bool
     */
    public function saveUploadedFile(array $routes, $rename = false, $allowedFileTypes = null)
    {
        $year = date('Y', time());
        $month = date('m', time());
        $structure = "{$routes['baseUrl']}/{$routes['uploadPath']}/$year/$month";
        $basePath = Yii::getAlias($routes['basePath']);
        $absolutePath = "$basePath/$structure";

        // create actual directory structure "yyyy/mm"
        if (!file_exists($absolutePath)) {
            mkdir($absolutePath, 0777, true);
        }

        // get file instance
        $this->file = UploadedFile::getInstance($this, 'file');

        if ($allowedFileTypes === null) {
            $allowedFileTypes = Yii::$app->getModule('media')->allowedFileTypes;
        }

        if (!empty($allowedFileTypes) && is_array($allowedFileTypes) && !in_array($this->file->type, $allowedFileTypes)) {
            throw new \Exception(Yii::t('core/media', 'Sorry, [{filetype}] file type is not permitted!', ['filetype' => $this->file->type]));
        }

        //if a file with the same name already exist append a number
        $counter = 0;
        do {
            if ($counter == 0)
                $filename = Inflector::slug($this->file->baseName) . '.' . $this->file->extension;
            else {
                //if we don't want to rename we finish the call here
                if ($rename == false) return false;
                $filename = Inflector::slug($this->file->baseName) . $counter . '.' . $this->file->extension;
            }
            $url = "$structure/$filename";
            $counter++;
        } while (self::findByUrl($url)); // checks for existing url in db
        // save original uploaded file
        $this->file->saveAs("$absolutePath/$filename");
        $this->filename = $filename;
        $this->title = $filename;
        $this->alt = $filename;
        $this->type = $this->file->type;
        $this->size = $this->file->size;
        $this->url = $url;
        if(Yii::$app->core->isMultilingual){
            $this->title_es = $filename;
            $this->title_en = $filename;
            $this->alt_es = $filename;
            $this->alt_en = $filename;
        }

        return $this->save();
    }

    /**
     * Create thumbs for this image
     *
     * @param array $routes see routes in module config
     * @param array $presets thumbs presets. See in module config
     * @return bool
     */
    public function createThumbs(array $routes, array $presets)
    {
        $thumbs = [];
        $basePath = $basePath = Yii::getAlias($routes['basePath']);
        $originalFile = pathinfo($this->url);
        $dirname = $originalFile['dirname'];
        $filename = $originalFile['filename'];
        $extension = $originalFile['extension'];

        Imagine::$driver = [Imagine::DRIVER_GD2, Imagine::DRIVER_GMAGICK, Imagine::DRIVER_IMAGICK];

        foreach ($presets as $alias => $preset) {
            $width = $preset['size'][0];
            $height = $preset['size'][1];

            $thumbUrl = "$dirname/$filename-{$width}x{$height}.$extension";

            Imagine::thumbnail("$basePath/{$this->url}", $width, $height)->save("$basePath/$thumbUrl");

            $thumbs[$alias] = $thumbUrl;
        }

        $this->thumbs = serialize($thumbs);
        $this->detachBehavior('timestamp');

        // create default thumbnail
        $this->createDefaultThumb($routes);

        return $this->save();
    }

    /**
     * Create default thumbnail
     *
     * @param array $routes see routes in module config
     */
    public function createDefaultThumb(array $routes)
    {
        $originalFile = pathinfo($this->url);
        $dirname = $originalFile['dirname'];
        $filename = $originalFile['filename'];
        $extension = $originalFile['extension'];

        Imagine::$driver = [Imagine::DRIVER_GD2, Imagine::DRIVER_GMAGICK, Imagine::DRIVER_IMAGICK];

        $size = MediaModule::getDefaultThumbSize();
        $width = $size[0];
        $height = $size[1];
        $thumbUrl = "$dirname/$filename-{$width}x{$height}.$extension";
        $basePath = Yii::getAlias($routes['basePath']);
        Imagine::thumbnail("$basePath/{$this->url}", $width, $height)->save("$basePath/$thumbUrl");
    }


    /**
     * @return bool if type of this media file is image, return true;
     */
    public function isImage()
    {
        return in_array($this->type, self::$imageFileTypes);
    }

    /**
     * @param $baseUrl
     * @return string default thumbnail for image
     */
    public function getDefaultThumbUrl($baseUrl = '')
    {
        if ($this->isImage()) {
            $size = MediaModule::getDefaultThumbSize();
            $originalFile = pathinfo($this->url);
            $dirname = $originalFile['dirname'];
            $filename = $originalFile['filename'];
            $extension = $originalFile['extension'];
            $width = $size[0];
            $height = $size[1];

            return "$dirname/$filename-{$width}x{$height}.$extension";
        }

        return null;
    }

    /**
     * @return array thumbnails
     */
    public function getThumbs()
    {
        return unserialize($this->thumbs);
    }

    /**
     * @param string $alias thumb alias
     * @return string thumb url
     */
    public function getThumbUrl($alias)
    {
        $thumbs = $this->getThumbs();

        if ($alias === 'original') {
            return $this->url;
        }

        return !empty($thumbs[$alias]) ? $thumbs[$alias] : '';
    }

    /**
     * Thumbnail image html tag
     *
     * @param string $alias thumbnail alias
     * @param array $options html options
     * @return string Html image tag
     */
    public function getThumbImage($alias, $options = [])
    {
        $url = $this->getThumbUrl($alias);

        if (empty($url)) {
            return '';
        }

        if (empty($options['alt'])) {
            $options['alt'] = $this->alt;
        }

        return Html::img($url, $options);
    }

    /**
     * @param MediaModule $module
     * @return array images list
     */
    public function getImagesList(MediaModule $module)
    {
        $thumbs = $this->getThumbs();
        $list = [];

        foreach ($thumbs as $alias => $url) {
            $preset = $module->thumbs[$alias];
            $list[$url] = Yii::t('core/media', $preset['name']) . ' ' . $preset['size'][0] . ' × ' . $preset['size'][1];
        }

        $originalImageSize = $this->getOriginalImageSize($module->routes);
        $list[$this->url] = Yii::t('core/media', 'Original') . ' ' . $originalImageSize;

        return $list;
    }

    /**
     * Delete thumbnails for current image
     * @param array $routes see routes in module config
     */
    public function deleteThumbs(array $routes)
    {
        $basePath = Yii::getAlias($routes['basePath']);

        foreach ($this->getThumbs() as $thumbUrl) {
            unlink("$basePath/$thumbUrl");
        }

        unlink("$basePath/{$this->getDefaultThumbUrl()}");
    }

    /**
     * Delete file
     * @param array $routes see routes in module config
     * @return bool
     */
    public function deleteFile(array $routes)
    {
        $basePath = Yii::getAlias($routes['basePath']);
        return unlink("$basePath/{$this->url}");
    }

    /**
     * @return int last changes timestamp
     */
    public function getLastChanges()
    {
        return !empty($this->updated_at) ? $this->updated_at : $this->created_at;
    }

    /**
     * This method wrap getimagesize() function
     * @param array $routes see routes in module config
     * @param string $delimiter delimiter between width and height
     * @return string image size like '1366x768'
     */
    public function getOriginalImageSize(array $routes, $delimiter = ' × ')
    {
        $imageSizes = $this->getOriginalImageSizes($routes);
        return "$imageSizes[0]$delimiter$imageSizes[1]";
    }

    /**
     * This method wrap getimagesize() function
     * @param array $routes see routes in module config
     * @return array
     */
    public function getOriginalImageSizes(array $routes)
    {
        $basePath = Yii::getAlias($routes['basePath']);
        return getimagesize("$basePath/{$this->url}");
    }

    /**
     * @return string file size
     */
    public function getFileSize()
    {
        Yii::$app->formatter->sizeFormatBase = 1000;
        return Yii::$app->formatter->asShortSize($this->size, 1);
    }

    /**
     * Find model by url
     *
     * @param $url
     * @return static
     */
    public static function findByUrl($url)
    {
        return self::findOne(['url' => $url]);
    }

    /**
     * Search models by file types
     * @param array $types file types
     * @return array|\common\db\ActiveRecord[]
     */
    public static function findByTypes(array $types)
    {
        return self::find()->filterWhere(['in', 'type', $types])->all();
    }

    public function getCreatedDate()
    {
        return Yii::$app->formatter->asDate(($this->isNewRecord) ? time() : $this->created_at);
    }

    public function getCreatedTime()
    {
        return Yii::$app->formatter->asTime(($this->isNewRecord) ? time() : $this->created_at);
    }

    public function getCreatedDatetime()
    {
        return "{$this->createdDate} {$this->createdTime}";
    }

    public static function find()
    {
        return new MultilingualQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullMediaAccess';
    }

    /**
     *
     * @inheritdoc
     */
    public static function getOwnerField()
    {
        return 'created_by';
    }

    public function getAlbum()
    {
        return $this->hasOne(Album::className(), ['id' => 'album_id']);
    }

    /**
     * This method used by the media viewer to show icon to the document
     * @return string fa-fa-icon for documents
     */
    public function getFA_Array(){
        switch($this->type){
            case 'application/msword':
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
            case 'application/vnd.ms-word.document.macroenabled.12':
                return ['name' => "fa fa-file-word-o", 'color' => '#2a5699'];
            case 'application/vnd.ms-excel':
            case 'application/vnd.ms-excel.addin.macroenabled.12':
            case 'application/x-xliff+xml':
            case 'application/vnd.ms-excel.sheet.binary.macroenabled.12':
            case 'application/vnd.ms-excel.sheet.macroenabled.12':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
            case 'application/vnd.ms-excel.template.macroenabled.12':
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.template':
                return ['name' => "fa fa-file-excel-o", 'color' => '#008040'];
            case 'application/vnd.ms-powerpoint.template.macroenabled.12':
            case 'application/vnd.openxmlformats-officedocument.presentationml.template':
            case 'application/vnd.ms-powerpoint.addin.macroenabled.12':
            case 'application/vnd.ms-powerpoint':
            case 'application/vnd.ms-powerpoint.slideshow.macroenabled.12':
            case 'application/vnd.openxmlformats-officedocument.presentationml.slideshow':
            case 'application/vnd.ms-powerpoint.presentation.macroenabled.12':
            case 'application/vnd.openxmlformats-officedocument.presentationml.presentation':
                return ['name' => "fa fa-file-powerpoint-o", 'color' => '#d04727'];
            case 'application/pdf':
                return ['name' => "fa fa-file-pdf-o", 'color' => '#f43c3c'];
            case 'audio/mpeg':
            case 'audio/mp3';
            case 'audio/mp4':
            case 'audio/ogg':
            case 'audio/x-wav':
            case 'audio/webm':
            case 'audio/x-ms-wma':
                return ['name' => "fa fa-file-sound-o", 'color' => '#149e82'];
            case 'text/plain':
            case 'text/csv':
            case 'application/vnd.oasis.opendocument.text':
            case 'application/vnd.oasis.opendocument.text-template':
            case 'text/richtext':
                return ['name' => "fa fa-file-text-o", 'color' => '#4e4e4e'];
            case 'application/zip':
            case 'application/x-rar-compressed':
            case 'application/octet-stream':
            case 'application/x-7z-compressed':
                return ['name' => "fa fa-file-zip-o", 'color' => '#ebb943'];
            case 'video/x-flv':
            case 'video/quicktime':
            case 'video/x-sgi-movie':
            case 'video/mp4':
            case 'video/mpeg':
            case 'video/webm':
            case 'video/h261':
            case 'video/h263':
            case 'video/h264':
                return ['name' => "fa fa-file-movie-o", 'color' => '#ef5501'];
            case 'text/x-java-source':
            case 'application/java-archive':
            case 'application/javascript':
            case 'application/json':
            case 'application/xhtml+xml':
            case 'application/xv+xml':
            case 'text/html':
            case 'text/css':
            case 'text/x-pascal':
            case 'application/java-vm':
                return ['name' => "fa fa-file-code-o", 'color' => '#f26651'];
            case 'application/x-sql':
            case 'application/msaccess':
            case 'application/x-msaccess':
                return ['name' => "fa fa-database", 'color' => '#717171'];
            case 'image/svg+xml':
                return ['name' => "fa fa-file-photo-o", 'color' => '#008299'];
            default:
                return ['name' => "fa fa-file-archive-o", 'color' => '#c49c5a'];
        }
    }
}