<?php

namespace common\settings\controllers;

/**
 * DefaultController implements General Settings page.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class DefaultController extends SettingsBaseController
{
    public $modelClass = 'common\settings\models\GeneralSettings';
    public $viewPath = '@common/settings/views/default/index';

}