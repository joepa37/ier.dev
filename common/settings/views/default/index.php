<?php

use common\helpers\Html;
use common\settings\assets\SettingsAsset;
use common\settings\models\GeneralSettings;
use common\widgets\ActiveForm;
use common\widgets\LanguagePills;

/* @var $this yii\web\View */
/* @var $model common\models\Setting */
/* @var $form common\widgets\ActiveForm */

$this->title = Yii::t('core/settings', 'General Settings');
$this->params['breadcrumbs'][] = $this->title;

SettingsAsset::register($this);
?>
<div class="setting-index">

    <div class="setting-form">
        <?php
        $form = ActiveForm::begin([
            'id' => 'setting-form',
            'validateOnBlur' => false,
        ])
        ?>

        <div class="row">
            <div class="col-md-9">
                <div class="box box-primary">
                    <div class="panel-body">

                        <?php if ($model->isMultilingual()): ?>
                            <?= LanguagePills::widget() ?>
                        <?php endif; ?>

                        <?= $form->field($model, 'title', ['multilingual' => true])->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'description', ['multilingual' => true])->textInput(['maxlength' => true]) ?>

                        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->hint($model->getDescription('email')) ?>

                        <?= $form->field($model, 'timezone', ['options' => ['class' => 'form-group select-field']])
                            ->dropDownList(GeneralSettings::getTimezones())->hint($model->getDescription('timezone')) ?>

                        <?= $form->field($model, 'dateformat', ['options' => ['class' => 'form-group select-field']])
                            ->dropDownList(GeneralSettings::getDateFormats())->hint($model->getDescription('dateformat')) ?>

                        <?= $form->field($model, 'timeformat', ['options' => ['class' => 'form-group select-field']])
                            ->dropDownList(GeneralSettings::getTimeFormats())->hint($model->getDescription('timeformat')) ?>

                        <?= $form->field($model, 'displaydateformat', ['options' => ['class' => 'form-group select-field']])
                            ->dropDownList(GeneralSettings::getDisplayDateFormats())->hint($model->getDescription('displaydateformat')) ?>

                        <?= $form->field($model, 'savedateformat', ['options' => ['class' => 'form-group select-field']])
                            ->dropDownList(GeneralSettings::getSaveDateFormats())->hint($model->getDescription('savedateformat')) ?>

                    </div>
                </div>
            </div>

            <div class="col-md-3">
                <div class="box box-success">
                    <div class="panel-body">
                        <div class="record-info">
                            <div class="form-group">
                                <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => 'btn btn-primary']) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/settings'], ['class' => 'btn btn-default']) ?>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php ActiveForm::end(); ?>

    </div>
</div>


