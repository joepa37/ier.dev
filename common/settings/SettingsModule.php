<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 */

namespace common\settings;

/**
 * Settings Module For IER
 *
 * @author José Peña <joepa37@gmail.com>
 */
class SettingsModule extends \yii\base\Module
{
    /**
     * Version number of the module.
     */
    const VERSION = '0.1.0';

    /**
     * @inheritdoc
     */
    public $controllerNamespace = 'common\settings\controllers';

}