<?php

namespace common\settings\assets;

use yii\web\AssetBundle;

/**
 * SettingsAsset.
 *
 * @author José Peña <joepa37@gmail.com>
 */
class SettingsAsset extends AssetBundle
{
    public $sourcePath = '@common/settings/assets/source';
    public $css = [
        'css/settings.css',
    ];
    public $depends = [
        'yii\bootstrap\BootstrapAsset',
        'yii\web\JqueryAsset',
        'common\assets\YeeAsset'
    ];

}