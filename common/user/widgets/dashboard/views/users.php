<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
?>

<div class="box box-danger">
    <div class="box-header with-border">
        <i class="fa fa-users"></i>
        <h3 class="box-title"><?= Yii::t('core/post', 'Users') ?></h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
            </button>
        </div>
    </div>
    <div class="box-body no-padding">
        <?php if (count($recent)): ?>
            <ul class="users-list clearfix">
                <?php foreach ($recent as $item) : ?>
                    <li>
                        <?php
                            $bundle = \backend\assets\BackendAsset::register($this);
                            $defaultAvatar = $bundle->baseUrl . '/img/No profile picture.png';
                            $options = [
                                'style' => 'height: auto; width: 80px; margin-right: 10px;',
                                'alt' => 'User Avatar',
                            ];
                            $avatar = ($userAvatar = $item->getAvatar('large')) ? $userAvatar : $defaultAvatar;
                            echo Html::a(\common\helpers\Html::img($avatar, $options), ['/user/default/update', 'id' => $item->id]);
                        ?>
                        <?php
                            $options = [
                                'class' => 'users-list-name',
                            ];
                            echo Html::a($item->fullName, ['/user/default/update', 'id' => $item->id], $options); ?>
                        <span class="users-list-date" style="overflow: hidden; text-overflow: ellipsis;"><?= $item->email ?></span>
                    </li>
                <?php endforeach; ?>
            </ul>
    </div>
    <div class="box-footer text-center">
        <a class="uppercase">
            <?php $list = [] ?>
            <?php foreach ($users as $item) : ?>
                <?php $list[] = Html::a("<b>{$item['count']}</b> {$item['label']}", $item['url']); ?>
            <?php endforeach; ?>
            <?= implode('<span style="color: #999; margin-left: 5px; margin-right: 5px;"> | </span>', $list) ?>
        </a>
        <?php else: ?>
            <h4><em><?= Yii::t('core/post', 'No users found.') ?></em></h4>
        <?php endif; ?>
    </div>
</div>