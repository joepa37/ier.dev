<?php

use yii\helpers\Html;
use common\comments\widgets\Comments;

/**
 * @var yii\web\View $this
 * @var common\models\User $model
 */
$this->title = Yii::t('core/user', 'Update User');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core/user', 'Permissions'), ['user-permission/set', 'id' => $model->id], ['class' => 'btn btn-sm btn-primary'])
];
?>

<div class="user-update">
    <?= $this->render('_form', compact('model')) ?>

    <?php echo Comments::widget(['model' => \common\models\User::className(), 'model_id' => $model->id]); ?>
</div>