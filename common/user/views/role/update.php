<?php
/**
 * @var common\widgets\ActiveForm $form
 * @var common\models\Role $model
 */

use yii\helpers\Html;

$this->title = Yii::t('core/user', 'Update Role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Roles'), 'url' => ['/user/role/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="role-update">
    <?= $this->render('_form', compact('model')) ?>
</div>