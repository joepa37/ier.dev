<?php

use common\grid\columns\DateRangePicker\DateRangePicker;
use common\grid\GridPageSize;
use common\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;
use yii\helpers\Url;

/* @var $this yii\web\View
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\user\models\search\UserActivityLogSearch $searchModel
 */


$this->title = Yii::t('core/user', 'Activity Log');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-activity-log-index">
    
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId'=>'user-activity-log-grid-pjax']) ?>
                </div>
            </div>
            
            <?php Pjax::begin([
                'id'=>'user-activity-log-grid-pjax',
            ]) ?>
            
            <?= GridView::widget([
                'id'=>'user-activity-log-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActions' => ' ',
                'columns' => [
                        [
                            'attribute'=>'user_id',
                            'class' => 'common\grid\columns\TitleActionColumn',
                            'controller' => '/user/activity-log',
                            'title' => function ($model) {
                                return Html::a(
                                    isset($model->user) ? $model->user->username : Yii::t('core/user', 'Guest'), ['/user/activity-log/view', 'id'=>$model->id], ['data-pjax'=>0]);
                            },
                            'buttonsTemplate' => '{view}',
                            'buttons' => [
                                'view' => function ($url, $model, $key) {
                                    return Html::a(Yii::t('core/user', 'View Activity Log'),
                                        Url::to(['activity-log/view', 'id' => $model->id]), [
                                            'title' => Yii::t('core/user', 'View Activity Log'),
                                            'data-pjax' => '0'
                                        ]
                                    );
                                },
                            ]
                        ],
                        'controller',
                        'action',
                        'param1',
                        'param2',
                        'os',
                        array(
                            'attribute' => 'ipaddress',
                            'value' => function ($model) {
                                return Html::a($model->ipaddress,
                                    "http://ipinfo.io/" . $model->ipaddress,
                                    ["target" => "_blank"]);
                            },
                            'format' => 'raw',
                        ),
                        //'logtime:datetime',
                        [
                            'attribute' => 'logtime',
                            'value' => function ($model) {
                                return $model->logDatetime;
                            },
                        ],
                ],
            ]); ?>
            
            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php DateRangePicker::widget([
        'model'     => $searchModel,
        'attribute' => 'logtime',
]) ?>
