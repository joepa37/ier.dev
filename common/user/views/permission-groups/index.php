<?php

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;
use common\models\User;
use yii\helpers\Url;
use yii\widgets\Pjax;

/**
 * @var yii\web\View $this
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var common\user\models\search\AuthItemGroupSearch $searchModel
 */
$this->title = Yii::t('core/user', 'Permission Groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['create'], ['class' => 'btn btn-sm btn-primary'])
];
?>

<div class="permission-groups-index">
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'permission-groups-grid-pjax']) ?>
                </div>
            </div>

            <?php
            Pjax::begin([
                'id' => 'permission-groups-grid-pjax',
            ])
            ?>

            <?=
            GridView::widget([
                'id' => 'permission-groups-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'permission-grid',
                    'actions' => [Url::to(['bulk-delete']) => Yii::t('core', 'Delete')]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'attribute' => 'name',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/user/permission-groups',
                        'title' => function ($model) {
                            if (User::hasPermission('manageRolesAndPermissions')) {
                                return Html::a(
                                    $model->name, ['update', 'id' => $model->code],
                                    ['data-pjax' => 0]
                                );
                            } else {
                                return $model->name;
                            }

                        },
                        'buttonsTemplate' => '{update} {delete}',
                    ],
                    'code',
                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>

































