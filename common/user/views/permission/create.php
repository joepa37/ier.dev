<?php
/**
 *
 * @var yii\web\View $this
 * @var common\widgets\ActiveForm $form
 * @var common\models\Permission $model
 */

use yii\helpers\Html;

$this->title = Yii::t('core/user', 'Create Permission');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Users'), 'url' => ['/user/default/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/user', 'Permissions'), 'url' => ['/user/permission/index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="permission-create">
    <?= $this->render('_form', compact('model')) ?>
</div>