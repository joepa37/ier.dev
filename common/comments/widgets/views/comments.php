<?php

use common\comments\Comments as CommentsModule;
use common\comments\Comments;
use common\comments\components\CommentsHelper;
use common\comments\models\Comment;
use common\comments\widgets\CommentsForm;
use yii\timeago\TimeAgo;
use yii\widgets\ListView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model common\comments\models\Comment */
$commentsPage = Yii::$app->getRequest()->get("comment-page", 1);
$cacheKey = 'comment' . $model . $model_id . $commentsPage;
$cacheProperties = CommentsHelper::getCacheProperties($model, $model_id);
?>

<?php if($is_box): ?>
    <div class="box box-warning">
        <div class="box-header with-border">
            <i class="fa fa-comments"></i>
            <?php if ($this->beginCache($cacheKey . '-count', $cacheProperties)) : ?>
                <h5 class="box-title"><?= Comments::t('comments', 'All Comments') ?> (<?= Comment::activeCount($model, $model_id) ?>)</h5>
                <?php $this->endCache(); ?>
            <?php endif; ?>
            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="box-body comments" style="padding: 0; margin: 10px 10px 13px 15px;">
<?php else: ?>
    <div class="comments-box" style="margin: 20px 10px 13px 15px;">
<?php endif; ?>

    <?php if (!Comments::getInstance()->onlyRegistered || !Yii::$app->user->isGuest): ?>
        <div class="comments-main-form">
            <?= CommentsForm::widget(); ?>
        </div>
    <?php endif; ?>

    <?php if ($this->beginCache($cacheKey, $cacheProperties)) : ?>
        <?php
            Pjax::begin();

            echo ListView::widget([
                'dataProvider' => $dataProvider,
                'emptyText' => '',
                'itemView' => function ($model, $key, $index, $widget) {
                    $nested_level = 1;
                    return $this->render('comment', compact('model', 'widget', 'nested_level'));
                },
                'options' => ['class' => 'comments'],
                'itemOptions' => ['class' => 'comment'],
                'layout' => '{items}<div class="text-center">{pager}</div>',
                'pager' => [
                    'class' => yii\widgets\LinkPager::className(),
                    'hideOnSinglePage' => true,
                    'firstPageLabel' => '<<',
                    'prevPageLabel' => '<',
                    'nextPageLabel' => '>',
                    'lastPageLabel' => '>>',
                    'options' => ['class' => 'pagination pagination-sm'],
                ],
            ]);

            Pjax::end();
            $this->endCache();
        ?>
    <?php else: ?>
        <?php TimeAgo::widget(); ?>
    <?php endif; ?>

    <?php if(Comment::activeCount($model, $model_id) <= 0) : ?>
        <h4 style="margin-bottom: 15px; margin-top: -10px;"><em><?= CommentsModule::t('comments', 'No comments.') ?></em></h4>
    <?php endif; ?>

    <?php if($is_box): ?>
        </div>
            </div>
    <?php else: ?>
        </div>
    <?php endif; ?>

