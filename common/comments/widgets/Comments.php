<?php

namespace common\comments\widgets;

use common\comments\assets\CommentsAsset;
use common\comments\Comments as CommentModule;
use common\comments\Comments as CommentsModule;
use common\comments\components\CommentsHelper;
use common\comments\models\Comment;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;

class Comments extends \yii\base\Widget
{
    public $model;
    public $model_id = 0;
    public $is_box = true;

    public function init()
    {
        parent::init();

        if ($this->model instanceof Model) {
            $this->model_id = $this->model->id;
            $this->model = $this->model->tableName();
        }
    }

    public function run()
    {
        $commentsAsset = CommentsAsset::register($this->getView());
        CommentModule::getInstance()->commentsAssetUrl = $commentsAsset->baseUrl;

        $model = $this->model;
        $model_id = $this->model_id;
        $is_box = $this->is_box;

        $comment = new Comment(compact('model', 'model_id'));
        $comment->scenario = (Yii::$app->user->isGuest) ? Comment::SCENARIO_GUEST : Comment::SCENARIO_USER;

        if ((!CommentModule::getInstance()->onlyRegistered || !Yii::$app->user->isGuest) && $comment->load(Yii::$app->getRequest()->post())) {

            if ($comment->validate() && Yii::$app->getRequest()->validateCsrfToken()
                && Yii::$app->getRequest()->getCsrfToken(true) && $comment->save()
            ) {
                if (Yii::$app->user->isGuest) {
                    CommentsHelper::setCookies([
                        'username' => $comment->username,
                        'email' => $comment->email,
                    ]);
                }

                Yii::$app->getResponse()->redirect(Yii::$app->request->referrer);
                return;
            }
        }

        $dataProvider = new ActiveDataProvider([
            'query' => Comment::find(true)->where([
                'model' => $model,
                'model_id' => $model_id,
                'parent_id' => NULL,
                'status' => Comment::STATUS_PUBLISHED,
            ]),
            'pagination' => [
                'pageSize' => CommentsModule::getInstance()->commentsPerPage,
                'pageParam' => 'comment-page',
                'pageSizeParam' => 'comments-per-page',
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => CommentsModule::getInstance()->orderDirection,
                ]
            ],
        ]);

        $this->registerCSS();

        return $this->render('comments', compact('model', 'model_id', 'is_box', 'comment', 'dataProvider'));
    }

    public function registerCSS(){
        $css = <<< CSS
            .pagination {
                margin-top: 15px;
                margin-bottom: 0;
            }
CSS;
        $this->getView()->registerCss($css);
    }
}