<?php

use common\grid\GridPageSize;
use common\grid\GridQuickLinks;
use common\grid\GridView;
use common\helpers\Html;
use common\models\User;
use common\page\models\Page;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\grid\columns\DateRangePicker\DateRangePicker;

/* @var $this yii\web\View */
/* @var $searchModel common\page\models\search\PageSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('core/page', 'Pages');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a(Yii::t('core', 'Add New'), ['/page/default/create'], ['class' => 'btn btn-sm btn-primary'])
];
?>
<div class="page-index">

    <div class="box box-primary">
        <div class="panel-body">

            <?php Pjax::begin(['id' => 'page-grid-pjax']) ?>

            <div class="row">
                <div class="col-sm-6">
                    <?= GridQuickLinks::widget([
                        'model' => Page::className(),
                        'searchModel' => $searchModel,
                        'labels' => [
                            'all' => Yii::t('core', 'All'),
                            'active' => Yii::t('core', 'Published'),
                            'inactive' => Yii::t('core', 'Pending'),
                        ],
                    ]) ?>
                </div>

                <div class="col-sm-6 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'page-grid-pjax']) ?>
                </div>
            </div>



            <?=
            GridView::widget([
                'id' => 'page-grid',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActionOptions' => [
                    'gridId' => 'page-grid',
                    'actions' => [
                        Url::to(['bulk-activate']) => Yii::t('core', 'Publish'),
                        Url::to(['bulk-deactivate']) => Yii::t('core', 'Unpublish'),
                        Url::to(['bulk-delete']) => Yii::t('yii', 'Delete'),
                    ]
                ],
                'columns' => [
                    ['class' => 'common\grid\CheckboxColumn', 'options' => ['style' => 'width:10px']],
                    [
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'controller' => '/page/default',
                        'title' => function (Page $model) {
                            return Html::a($model->title, ['/page/default/view', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                    ],
                    [
                        'attribute' => 'created_by',
                        'filter' => User::getUsersList(),
                        'value' => function (Page $model) {
                            return Html::a($model->author->username,
                                ['/user/default/update', 'id' => $model->created_by],
                                ['data-pjax' => 0]);
                        },
                        'format' => 'raw',
                        'visible' => User::hasPermission('viewUsers'),
                        'options' => ['style' => 'width:180px'],
                    ],
                    [
                        'class' => 'common\grid\columns\StatusColumn',
                        'attribute' => 'status',
                        'optionsArray' => Page::getStatusOptionsList(),
                        'options' => ['style' => 'width:60px'],
                    ],
                    [
                        'attribute' => 'published_at',
                        'value' => function ($model) {
                            return $model->publishedDateTime;
                        },
                        'options' => ['style' => 'width:150px'],
                    ],
                ],
            ]);
            ?>

            <?php Pjax::end() ?>
        </div>
    </div>
</div>

<?php
DateRangePicker::widget([
    'model' => $searchModel,
    'attribute' => 'published_at',
])
?>
