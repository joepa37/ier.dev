<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\page\models\Page */

$this->title = Yii::t('core', 'Update "{item}"', ['item' => $model->title]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/page', 'Pages'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core', 'Update');
?>

<div class="page-update">
    <?= $this->render('_form', compact('model')) ?>
</div>


