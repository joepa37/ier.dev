<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\seo\models\Seo */

$this->title = Yii::t('core/seo', 'Update SEO Record');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/seo', 'SEO'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="seo-update">
    <?= $this->render('_form', compact('model')) ?>
</div>


