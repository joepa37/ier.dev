<?php

namespace common\controllers\admin;

use yii\helpers\ArrayHelper;

class DashboardController extends BaseController
{
    /**
     * @inheritdoc
     */
    public $enableOnlyActions = ['index'];
    public $widgets = NULL;

    public function actions()
    {

        if ($this->widgets === NULL) {
            $this->widgets = [
                /*'common\widgets\dashboard\IER',
                [
                    'class' => 'common\user\widgets\dashboard\Users',
                    'position' => 'right',
                ],
                'common\post\widgets\dashboard\Posts',
                'common\media\widgets\dashboard\Media',
                [
                    'class' => 'common\comment\widgets\dashboard\Comments',
                    'position' => 'right',
                ],
                [
                    'class' => 'common\widgets\dashboard\Info',
                    'position' => 'right',
                ],*/
            ];
        }

        return ArrayHelper::merge(parent::actions(), [
            'index' => [
                'class' => 'common\web\DashboardAction',
                'widgets' => $this->widgets,
            ]
        ]);
    }
}