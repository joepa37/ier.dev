<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\icons;

use yii\web\AssetBundle;

class DemoIconsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/icons/source';
    public $css = [
        'css/demo-icons.css',
    ];
}
