<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\icons;

use yii\web\AssetBundle;

class SkyIconsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/icons/source';
    public $js = [
        'js/sky-icons.js',
    ];
}
