<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\icons;

use yii\web\AssetBundle;

class IonIconsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/icons/source';
    public $css = [
        'css/ion-icons.css',
    ];
}
