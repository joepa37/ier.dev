<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\pace;

use yii\web\AssetBundle;

class PaceAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/pace/source';
    public $css = [
        'css/pace.min.css',
    ];
    public $js = [
        'js/pace.min.js',
    ];
}
