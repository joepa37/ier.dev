<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\fonts;

use yii\web\AssetBundle;

class OpenSansFontAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/fonts/source';
    public $css = [
        'css/open-sans-font.css',
    ];
}
