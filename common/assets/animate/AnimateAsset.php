<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\animate;

use common\assets\fonts\OpenSansFontAsset;
use common\assets\Html5shiv;
use common\assets\icons\DemoIconsAsset;
use common\assets\icons\FontAwesomeAsset;
use common\assets\icons\IonIconsAsset;
use common\assets\icons\ThemifyIconsAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

class AnimateAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/animate/source';
    public $css = [
        'css/animate.css',
    ];
}
