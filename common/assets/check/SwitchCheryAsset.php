<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\check;

use yii\web\AssetBundle;

class MagicCheckAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/check/source';
    public $css = [
        'css/switch-chery.css',
    ];
}
