<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\bootbox;

use yii\web\AssetBundle;

class BootBoxAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/bootbox/source';
    public $js = [
        'js/bootbox.js',
    ];
}
