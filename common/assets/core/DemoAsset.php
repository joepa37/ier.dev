<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\core;

use common\assets\animate\AnimateAsset;
use common\assets\bootbox\BootBoxAsset;
use common\assets\fonts\OpenSansFontAsset;
use common\assets\icons\DemoIconsAsset;
use common\assets\icons\FontAwesomeAsset;
use common\assets\icons\IonIconsAsset;
use common\assets\icons\SkyIconsAsset;
use common\assets\icons\ThemifyIconsAsset;
use common\assets\notify\NotifyAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

class DemoAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/core/source';
    public $css = [
        'css/demo/core-demo.css',
    ];
    public $js = [
        'js/demo/core-demo.js',
        'js/demo/widgets.js',
    ];
}
