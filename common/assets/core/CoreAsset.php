<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\core;

use common\assets\animate\AnimateAsset;
use common\assets\bootbox\BootBoxAsset;
use common\assets\fonts\OpenSansFontAsset;
use common\assets\icons\DemoIconsAsset;
use common\assets\icons\FontAwesomeAsset;
use common\assets\icons\IonIconsAsset;
use common\assets\icons\SkyIconsAsset;
use common\assets\icons\ThemifyIconsAsset;
use common\assets\notify\NotifyAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

class CoreAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/core/source';
    public $css = [
        'css/core.css',
    ];
    public $js = [
        'js/core.js',
    ];
    public $depends = [
	    JqueryAsset::class,
	    YiiAsset::class,
	    BootstrapPluginAsset::class,
        OpenSansFontAsset::class,
	    DemoIconsAsset::class,
	    IonIconsAsset::class,
        SkyIconsAsset::class,
        ThemifyIconsAsset::class,
        FontAwesomeAsset::class,
        AnimateAsset::class,
        NotifyAsset::class,
	    DemoAsset::class,
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);
        $js = <<<JS
        coreBaseUrl = "$this->baseUrl";
JS;
        $view->registerJs($js, View::POS_HEAD);
    }
}
