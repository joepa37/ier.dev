
// Dashboard.js
// ====================================================================
// This file should not be included in your project.
// This is just a sample how to initialize plugins or components.
//
// - ThemeOn.net -


$(window).on('load', function() {
    // WELCOME NOTIFICATIONS
    // =================================================================
    // Require Admin Core Javascript
    // =================================================================
    $.niftyNoty({
        type: 'success',
        title: 'Welcome Administrator.',
        message: 'You will notice that you now have an admin menu<br> that appears on the right hand side.',
        container: 'floating',
        timer: 5000
    });
});