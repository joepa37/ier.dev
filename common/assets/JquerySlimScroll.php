<?php
namespace common\assets;

/**
 * Created by PhpStorm.
 * User: José Peña
 */

use yii\web\AssetBundle;

class JquerySlimScroll extends AssetBundle
{
    public $sourcePath = '@bower/jquery-slimscroll';
    public $js = [
        'jquery.slimscroll.min.js'
    ];
}
