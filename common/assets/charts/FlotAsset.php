<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\charts;

use yii\web\AssetBundle;

class FlotAsset extends AssetBundle
{
    public $sourcePath = '@bower/flot';
    public $js = [
        'jquery.flot.js'
    ];

    public $depends = [
        'common\assets\JqueryAsset',
    ];
}
