<?php

namespace common\assets\language;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class LanguagePillsAsset
 * 
 * @package common\assets
 */
class LanguagePillsAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/language/source';
    public $css = [
        'css/language-pills.css',
    ];
    public $js = [
        'js/language-pills.js',
    ];
    public $depends = [
        JqueryAsset::class,
    ];
}