<?php

namespace common\assets\language;

use yii\web\AssetBundle;
use yii\web\JqueryAsset;

/**
 * Class LanguageSelectorAsset
 * 
 * @package common\widgets\assets
 */
class LanguageSelectorAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/language/source';
    public $css = [
        'css/language-selector.css',
    ];
    public $js = [
        //'js/language.js',
    ];
    public $depends = [
        JqueryAsset::class,
    ];
}