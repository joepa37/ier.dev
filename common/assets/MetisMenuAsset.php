<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class MetisMenuAsset
 * @package common\assets
 */
class MetisMenuAsset extends AssetBundle
{
    public $sourcePath = '@bower/metisMenu/dist';
    public $js = ['metisMenu.js'];
    public $css = ['metisMenu.css'];
    public $depends = ['yii\web\JqueryAsset'];

}