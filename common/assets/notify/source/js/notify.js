/**
 * Created by José Peña
 */
! function(n) {
    "use strict";
    var e, i, s, a, t = {},
        o = !1,
        r = function() {
            var n = document.body || document.documentElement,
                e = n.style,
                t = void 0 !== e.transition || void 0 !== e.WebkitTransition;
            return t
        }();
    n.niftyNoty = function(l) {
        {
            var h, c = {
                    type: "primary",
                    icon: "",
                    title: "",
                    message: "",
                    closeBtn: !0,
                    container: "page",
                    floating: {
                        position: "top-right",
                        animationIn: "jellyIn",
                        animationOut: "fadeOut"
                    },
                    html: null,
                    focus: !0,
                    timer: 0,
                    onShow: function() {},
                    onShown: function() {},
                    onHide: function() {},
                    onHidden: function() {}
                },
                f = n.extend({}, c, l),
                d = n('<div class="alert-wrap"></div>'),
                u = function() {
                    var n = "";
                    return l && l.icon && (n = '<div class="media-left alert-icon"><i class="' + f.icon + '"></i></div>'), n
                },
                g = function() {
                    var n = f.closeBtn ? '<button class="close" type="button"><i class="pci-cross pci-circle"></i></button>' : "",
                        e = '<div class="alert alert-' + f.type + '" role="alert">' + n + '<div class="media">';
                    return f.html ? e + f.html + "</div></div>" : e + u() + '<div class="media-body"><h4 class="alert-title">' + f.title + '</h4><p class="alert-message">' + f.message + "</p></div></div>"
                }(),
                p = function() {
                    if(!d.is(":hover")){
                        return f.onHide(), "floating" === f.container && f.floating.animationOut && (d.removeClass(f.floating.animationIn).addClass(f.floating.animationOut), r || (f.onHidden(), d.remove())), d.removeClass("in").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(n) {
                            "max-height" == n.originalEvent.propertyName && (f.onHidden(), d.remove())
                        }), clearInterval(h), null
                    }
                },
                q = function() {
                    return f.onHide(), "floating" === f.container && f.floating.animationOut && (d.removeClass(f.floating.animationIn).addClass(f.floating.animationOut), r || (f.onHidden(), d.remove())), d.removeClass("in").on("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd", function(n) {
                        "max-height" == n.originalEvent.propertyName && (f.onHidden(), d.remove())
                    }), clearInterval(h), null
                },
                v = function(e) {
                    n("body, html").animate({
                        scrollTop: e
                    }, 300, function() {
                        d.addClass("in")
                    })
                };
            ! function() {
                if (f.onShow(), "page" === f.container) e || (e = n('<div id="page-alert"></div>'), a && a.length || (a = n("#content-container")), a.prepend(e)), i = e, f.focus && v(0);
                else if ("floating" === f.container) t[f.floating.position] || (t[f.floating.position] = n('<div id="floating-' + f.floating.position + '" class="floating-container"></div>'), s && a.length || (s = n("#container")), s.append(t[f.floating.position])), i = t[f.floating.position], f.floating.animationIn && d.addClass("in animated " + f.floating.animationIn), f.focus = !1;
                else {
                    var r = n(f.container),
                        l = r.children(".panel-alert"),
                        c = r.children(".panel-heading");
                    if (!r.length) return o = !1, !1;
                    l.length ? i = l : (i = n('<div class="panel-alert"></div>'), c.length ? c.after(i) : r.prepend(i)), f.focus && v(r.offset().top - 30)
                }
                return o = !0, !1
            }()
        }
        if (o) {
            if (i.append(d.html(g)), d.find('[data-dismiss="noty"]').one("click", q), f.closeBtn && d.find(".close").one("click", q), f.timer > 0 && (h = setInterval(p, f.timer)), !f.focus) var y = setInterval(function() {
                d.addClass("in"), clearInterval(y)
            }, 200);
            d.one("transitionend webkitTransitionEnd oTransitionEnd MSTransitionEnd animationend webkitAnimationEnd oAnimationEnd MSAnimationEnd", function(n) {
                "max-height" != n.originalEvent.propertyName && "animationend" != n.type || !o || (f.onShown(), o = !1)
            })
        }
    }
}(jQuery);