<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\notify;

use common\assets\fonts\OpenSansFontAsset;
use common\assets\Html5shiv;
use common\assets\icons\DemoIconsAsset;
use common\assets\icons\FontAwesomeAsset;
use common\assets\icons\IonIconsAsset;
use common\assets\icons\ThemifyIconsAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\View;
use yii\web\YiiAsset;

class NotifyAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/notify/source';
    public $css = [
        'css/notify.css',
    ];

    public $js = [
        'js/notify.js',
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        parent::registerAssetFiles($view);
        $js = <<<JS
        $(window).on('load', function()  {
             $.niftyNoty({
                type: 'primary',
                container: 'page',
                html: '<div class="media-left"><span class="icon-wrap icon-wrap-xs icon-circle alert-icon"><i class="demo-pli-information icon-2x"></i></span></div><div class="media-body"><h4 class="alert-title">Server Load Limited</h4><p class="alert-message">Database server has reached its daily capicity</p></div>',
                floating: {
                    position: "top-right",
                    animationIn: "bounceInDown",
                    animationOut: "fadeOut"
                },
                timer: 4000
            });
        });
JS;
        $view->registerJs($js, View::POS_READY);
    }
}
