<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace common\assets\particle;

use yii\web\AssetBundle;

class ParticleAssset extends AssetBundle
{
    public $sourcePath = '@common/assets/particle/source';
    public $js = [
        'js/TweenLite.min.js',
        'js/EasePack.min.js',
        'js/rAF.js',
        'js/Particle.js',
    ];
}
