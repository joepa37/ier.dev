<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\CurricularMalla */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Curricular Mallas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curricular-malla-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'Curricular Malla').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'modality.name',
                'label' => Yii::t('core/school', 'Modality')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerClassTable->totalCount){
    $gridColumnClassTable = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerClassTable,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Class Table')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnClassTable
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerCourse->totalCount){
    $gridColumnCourse = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'schoolPeriod.name',
                'label' => Yii::t('core/school', 'School Period')
            ],
            ];
    echo Gridview::widget([
        'dataProvider' => $providerCourse,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Course')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnCourse
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPartial->totalCount){
    $gridColumnPartial = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
            ];
    echo Gridview::widget([
        'dataProvider' => $providerPartial,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Partial')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnPartial
    ]);
}
?>
    </div>
</div>
