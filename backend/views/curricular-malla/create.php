<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\CurricularMalla */

$this->title = Yii::t('core/school', 'Create Curricular Malla');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Curricular Mallas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curricular-malla-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
