<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\CurricularMalla */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Curricular Mallas'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="curricular-malla-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('core/school', 'Curricular Malla').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('core/school', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('core/school', 'Will open the generated PDF file in a new window')
                ]
            )?>
            <?= Html::a(Yii::t('core/school', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('core/school', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('core/school', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('core/school', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
            'attribute' => 'modality.name',
            'label' => Yii::t('core/school', 'Modality'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerClassTable->totalCount){
    $gridColumnClassTable = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerClassTable,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-class-table']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('core/school', 'Class Table')),
        ],
        'columns' => $gridColumnClassTable
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerCourse->totalCount){
    $gridColumnCourse = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
            [
                'attribute' => 'schoolPeriod.name',
                'label' => Yii::t('core/school', 'School Period')
            ],
                ];
    echo Gridview::widget([
        'dataProvider' => $providerCourse,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-course']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('core/school', 'Course')),
        ],
        'columns' => $gridColumnCourse
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerPartial->totalCount){
    $gridColumnPartial = [
        ['class' => 'yii\grid\SerialColumn'],
            ['attribute' => 'id', 'visible' => false],
            'name',
                ];
    echo Gridview::widget([
        'dataProvider' => $providerPartial,
        'pjax' => true,
        'pjaxSettings' => ['options' => ['id' => 'kv-pjax-container-partial']],
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => '<span class="glyphicon glyphicon-book"></span> ' . Html::encode(Yii::t('core/school', 'Partial')),
        ],
        'columns' => $gridColumnPartial
    ]);
}
?>
    </div>
</div>
