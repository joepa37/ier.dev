<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Journey */

$this->title = Yii::t('core/school', 'Save As New {modelClass}: ', [
    'modelClass' => 'Journey',
]). ' ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Journeys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Save As New');
?>
<div class="journey-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
