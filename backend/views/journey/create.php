<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Journey */

$this->title = Yii::t('core/school', 'Create Journey');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Journeys'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="journey-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
