<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\County */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'County'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="county-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'County').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'state.name',
                'label' => Yii::t('core/school', 'State')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerParentOrTutor->totalCount){
    $gridColumnParentOrTutor = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        [
                'attribute' => 'nationality.name',
                'label' => Yii::t('core/school', 'Nationality')
            ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
                'address',
        'phone_number',
        'email:email',
        'occupation',
        'workplace',
        'workplace_phone_number',
        'avatar:ntext',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerParentOrTutor,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Parent Or Tutor')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnParentOrTutor
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerStudent->totalCount){
    $gridColumnStudent = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
                'attribute' => 'nationality.name',
                'label' => Yii::t('core/school', 'Nationality')
            ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
                'address',
        'phone_number',
        'email:email',
        [
                'attribute' => 'father.name',
                'label' => Yii::t('core/school', 'Father')
            ],
        [
                'attribute' => 'mother.name',
                'label' => Yii::t('core/school', 'Mother')
            ],
        [
                'attribute' => 'tutor.name',
                'label' => Yii::t('core/school', 'Tutor')
            ],
        'observations',
        'avatar:ntext',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerStudent,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Student')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnStudent
    ]);
}
?>
    </div>
</div>
