<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->students,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
                'attribute' => 'nationality.name',
                'label' => Yii::t('core/school', 'Nationality')
            ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        'address',
        'phone_number',
        'email:email',
        [
                'attribute' => 'father.name',
                'label' => Yii::t('core/school', 'Father')
            ],
        [
                'attribute' => 'mother.name',
                'label' => Yii::t('core/school', 'Mother')
            ],
        [
                'attribute' => 'tutor.name',
                'label' => Yii::t('core/school', 'Tutor')
            ],
        'observations',
        'avatar:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'student'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
