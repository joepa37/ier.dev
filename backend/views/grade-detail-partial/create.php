<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\GradeDetailPartial */

$this->title = Yii::t('core/school', 'Create Grade Detail Partial');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grade Detail Partials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-detail-partial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
