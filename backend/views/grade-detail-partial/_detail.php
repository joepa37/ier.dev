<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\GradeDetailPartial */

?>
<div class="grade-detail-partial-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Html::encode($model->id) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'gradeDetail.id',
            'label' => Yii::t('core/school', 'Grade Detail'),
        ],
        [
            'attribute' => 'partial.name',
            'label' => Yii::t('core/school', 'Partial'),
        ],
        'acumulative_1',
        'acumulative_2',
        'exam',
        'leveling',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>