<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\GradeDetailPartialSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-grade-detail-partial-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'grade_detail_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\GradeDetail::find()->orderBy('id')->asArray()->all(), 'id', 'id'),
        'options' => ['placeholder' => Yii::t('core/school', 'Choose Grade detail')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'partial_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\Partial::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('core/school', 'Choose Partial')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'acumulative_1')->textInput(['placeholder' => 'Acumulative 1']) ?>

    <?= $form->field($model, 'acumulative_2')->textInput(['placeholder' => 'Acumulative 2']) ?>

    <?php /* echo $form->field($model, 'exam')->textInput(['placeholder' => 'Exam']) */ ?>

    <?php /* echo $form->field($model, 'leveling')->textInput(['placeholder' => 'Leveling']) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('core/school', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('core/school', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
