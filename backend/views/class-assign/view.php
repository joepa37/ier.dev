<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\ClassAssign */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Class Assigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="class-assign-view">

    <div class="row">
        <div class="col-sm-8">
            <h2><?= Yii::t('core/school', 'Class Assign').' '. Html::encode($this->title) ?></h2>
        </div>
        <div class="col-sm-4" style="margin-top: 15px">
<?=             
             Html::a('<i class="fa glyphicon glyphicon-hand-up"></i> ' . Yii::t('core/school', 'PDF'), 
                ['pdf', 'id' => $model->id],
                [
                    'class' => 'btn btn-danger',
                    'target' => '_blank',
                    'data-toggle' => 'tooltip',
                    'title' => Yii::t('core/school', 'Will open the generated PDF file in a new window')
                ]
            )?>
            <?= Html::a(Yii::t('core/school', 'Save As New'), ['save-as-new', 'id' => $model->id], ['class' => 'btn btn-info']) ?>            
            <?= Html::a(Yii::t('core/school', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
            <?= Html::a(Yii::t('core/school', 'Delete'), ['delete', 'id' => $model->id], [
                'class' => 'btn btn-danger',
                'data' => [
                    'confirm' => Yii::t('core/school', 'Are you sure you want to delete this item?'),
                    'method' => 'post',
                ],
            ])
            ?>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
            'attribute' => 'teacher.username',
            'label' => Yii::t('core/school', 'Teacher'),
        ],
        [
            'attribute' => 'section.name',
            'label' => Yii::t('core/school', 'Section'),
        ],
        [
            'attribute' => 'classTable.name',
            'label' => Yii::t('core/school', 'Class Table'),
        ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
</div>
