<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SectionSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;

$this->title = Yii::t('core/school', 'Sections');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Courses'), 'url' => ['/course/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
];
?>
<div class="section-index">
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'grid-section-pjax']) ?>
                </div>
            </div>

            <?=
            GridView::widget([
                'id' => 'grid-section',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActions' => ' ',
                'columns' => [
                    [
                        'attribute' => 'name',
                        'controller' => '/section',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'title' => function (\backend\models\Section $model) {
                            return Html::a($model->name, ['/section/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update}',
                        'options' => ['style' => 'width:90px'],
                    ],
                    [
                        'attribute' => 'school_period_id',
                        'label' => Yii::t('core/school', 'School Period'),
                        'value' => function($model){
                            return $model->course->schoolPeriod->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\SchoolPeriod::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-section-search-school_period_id']
                    ],
                    [
                        'attribute' => 'modality_id',
                        'label' => Yii::t('core/school', 'Modality'),
                        'value' => function($model){
                            return $model->course->curricularMalla->modality->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Modality::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-section-search-modality_id']
                    ],
                    [
                        'attribute' => 'course_id',
                        'label' => Yii::t('core/school', 'Course'),
                        'value' => function($model){
                            return $model->course->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Course::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-section-search-course_id']
                    ],
                    [
                        'attribute' => 'journey_id',
                        'label' => Yii::t('core/school', 'Journey'),
                        'value' => function($model){
                            return $model->journey->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Journey::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-section-search-journey_id']
                    ],

                ],
                'pjax' => true,
            ]);
            ?>

        </div>
    </div>
</div>
