<?php

use common\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Section */

$this->title = Yii::t('core', 'Update');
$this->params['subtitle'] = Yii::t('core/school', 'Section {name}', [
    'name' => $model->name,
]);
$this->params['subtitle'] = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = ['label' => $model->course->schoolPeriod->name, 'url' => ['/school-period/update', 'id' => $model->course->schoolPeriod->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Courses'), 'url' => ['/course/index']];
$this->params['breadcrumbs'][] = ['label' => $model->course->name, 'url' => ['/course/update', 'id' => $model->course->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Sections'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Sections'), ['/section/index'], ['class' => common\Core::LIST_CLASS])
];
?>
<div class="section-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
