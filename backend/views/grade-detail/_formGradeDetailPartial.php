<div class="form-group" id="add-grade-detail-partial">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'GradeDetailPartial',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'partial_id' => [
            'label' => 'Partial',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Partial::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('core/school', 'Choose Partial')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'acumulative_1' => ['type' => TabularForm::INPUT_TEXT],
        'acumulative_2' => ['type' => TabularForm::INPUT_TEXT],
        'exam' => ['type' => TabularForm::INPUT_TEXT],
        'leveling' => ['type' => TabularForm::INPUT_TEXT],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('core/school', 'Delete'), 'onClick' => 'delRowGradeDetailPartial(' . $key . '); return false;', 'id' => 'grade-detail-partial-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('core/school', 'Add Grade Detail Partial'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowGradeDetailPartial()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

