<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\GradeDetail */

$this->title = Yii::t('core/school', 'Create Grade Detail');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grade Details'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-detail-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
