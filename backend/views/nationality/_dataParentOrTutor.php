<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->parentOrTutors,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        [
                'attribute' => 'state.name',
                'label' => Yii::t('core/school', 'State')
            ],
        [
                'attribute' => 'county.name',
                'label' => Yii::t('core/school', 'County')
            ],
        'address',
        'phone_number',
        'email:email',
        'occupation',
        'workplace',
        'workplace_phone_number',
        'avatar:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'parent-or-tutor'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
