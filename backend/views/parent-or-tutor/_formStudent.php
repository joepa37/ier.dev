<div class="form-group" id="add-student">
<?php
use kartik\grid\GridView;
use kartik\builder\TabularForm;
use yii\data\ArrayDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

$dataProvider = new ArrayDataProvider([
    'allModels' => $row,
    'pagination' => [
        'pageSize' => -1
    ]
]);
echo TabularForm::widget([
    'dataProvider' => $dataProvider,
    'formName' => 'Student',
    'checkboxColumn' => false,
    'actionColumn' => false,
    'attributeDefaults' => [
        'type' => TabularForm::INPUT_TEXT,
    ],
    'attributes' => [
        "id" => ['type' => TabularForm::INPUT_HIDDEN, 'visible' => false],
        'name' => ['type' => TabularForm::INPUT_TEXT],
        'dni' => ['type' => TabularForm::INPUT_TEXT],
        'status' => ['type' => TabularForm::INPUT_TEXT],
        'nationality_id' => [
            'label' => 'Nationality',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\Nationality::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('core/school', 'Choose Nationality')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'gender_id' => ['type' => TabularForm::INPUT_TEXT],
        'birth_day' => ['type' => TabularForm::INPUT_TEXT],
        'birth_month' => ['type' => TabularForm::INPUT_TEXT],
        'birth_year' => ['type' => TabularForm::INPUT_TEXT],
        'pob' => ['type' => TabularForm::INPUT_TEXT],
        'county_id' => [
            'label' => 'County',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\County::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('core/school', 'Choose County')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'address' => ['type' => TabularForm::INPUT_TEXT],
        'phone_number' => ['type' => TabularForm::INPUT_TEXT],
        'email' => ['type' => TabularForm::INPUT_TEXT],
        'father_id' => [
            'label' => 'Parent or tutor',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\ParentOrTutor::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('core/school', 'Choose Parent or tutor')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'mother_id' => [
            'label' => 'Parent or tutor',
            'type' => TabularForm::INPUT_WIDGET,
            'widgetClass' => \kartik\widgets\Select2::className(),
            'options' => [
                'data' => \yii\helpers\ArrayHelper::map(\backend\models\ParentOrTutor::find()->orderBy('name')->asArray()->all(), 'id', 'name'),
                'options' => ['placeholder' => Yii::t('core/school', 'Choose Parent or tutor')],
            ],
            'columnOptions' => ['width' => '200px']
        ],
        'observations' => ['type' => TabularForm::INPUT_TEXT],
        'avatar' => ['type' => TabularForm::INPUT_TEXTAREA],
        'del' => [
            'type' => 'raw',
            'label' => '',
            'value' => function($model, $key) {
                return Html::a('<i class="glyphicon glyphicon-trash"></i>', '#', ['title' =>  Yii::t('core/school', 'Delete'), 'onClick' => 'delRowStudent(' . $key . '); return false;', 'id' => 'student-del-btn']);
            },
        ],
    ],
    'gridSettings' => [
        'panel' => [
            'heading' => false,
            'type' => GridView::TYPE_DEFAULT,
            'before' => false,
            'footer' => false,
            'after' => Html::button('<i class="glyphicon glyphicon-plus"></i>' . Yii::t('core/school', 'Add Student'), ['type' => 'button', 'class' => 'btn btn-success kv-batch-create', 'onClick' => 'addRowStudent()']),
        ]
    ]
]);
echo  "    </div>\n\n";
?>

