<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\ParentOrTutor */

$this->title = Yii::t('core/school', 'Create Parent Or Tutor');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Parent Or Tutor'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="parent-or-tutor-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
