<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\search\ParentOrTutorSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-parent-or-tutor-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id', ['template' => '{input}'])->textInput(['style' => 'display:none']); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'placeholder' => 'Name']) ?>

    <?= $form->field($model, 'dni')->textInput(['maxlength' => true, 'placeholder' => 'Dni']) ?>

    <?= $form->field($model, 'nationality_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\Nationality::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('core/school', 'Choose Nationality')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); ?>

    <?= $form->field($model, 'gender_id')->textInput(['placeholder' => 'Gender']) ?>

    <?php /* echo $form->field($model, 'birth_day')->textInput(['placeholder' => 'Birth Day']) */ ?>

    <?php /* echo $form->field($model, 'birth_month')->textInput(['placeholder' => 'Birth Month']) */ ?>

    <?php /* echo $form->field($model, 'birth_year')->textInput(['placeholder' => 'Birth Year']) */ ?>

    <?php /* echo $form->field($model, 'pob')->textInput(['maxlength' => true, 'placeholder' => 'Pob']) */ ?>

    <?php /* echo $form->field($model, 'county_id')->widget(\kartik\widgets\Select2::classname(), [
        'data' => \yii\helpers\ArrayHelper::map(\backend\models\County::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
        'options' => ['placeholder' => Yii::t('core/school', 'Choose County')],
        'pluginOptions' => [
            'allowClear' => true
        ],
    ]); */ ?>

    <?php /* echo $form->field($model, 'address')->textInput(['maxlength' => true, 'placeholder' => 'Address']) */ ?>

    <?php /* echo $form->field($model, 'phone_number')->textInput(['maxlength' => true, 'placeholder' => 'Phone Number']) */ ?>

    <?php /* echo $form->field($model, 'email')->textInput(['maxlength' => true, 'placeholder' => 'Email']) */ ?>

    <?php /* echo $form->field($model, 'occupation')->textInput(['maxlength' => true, 'placeholder' => 'Occupation']) */ ?>

    <?php /* echo $form->field($model, 'workplace')->textInput(['maxlength' => true, 'placeholder' => 'Workplace']) */ ?>

    <?php /* echo $form->field($model, 'workplace_phone_number')->textInput(['maxlength' => true, 'placeholder' => 'Workplace Phone Number']) */ ?>

    <?php /* echo $form->field($model, 'avatar')->textarea(['rows' => 6]) */ ?>

    <div class="form-group">
        <?= Html::submitButton(Yii::t('core/school', 'Search'), ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton(Yii::t('core/school', 'Reset'), ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
