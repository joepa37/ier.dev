<?php
use kartik\grid\GridView;
use yii\data\ArrayDataProvider;

    $dataProvider = new ArrayDataProvider([
        'allModels' => $model->students,
        'key' => 'id'
    ]);
    $gridColumns = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
                'attribute' => 'nationality.name',
                'label' => Yii::t('core/school', 'Nationality')
            ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        [
                'attribute' => 'county.name',
                'label' => Yii::t('core/school', 'County')
            ],
        'address',
        'phone_number',
        'email:email',
                        'observations',
        'avatar:ntext',
        [
            'class' => 'yii\grid\ActionColumn',
            'controller' => 'student'
        ],
    ];
    
    echo GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => $gridColumns,
        'containerOptions' => ['style' => 'overflow: auto'],
        'pjax' => true,
        'beforeHeader' => [
            [
                'options' => ['class' => 'skip-export']
            ]
        ],
        'export' => [
            'fontAwesome' => true
        ],
        'bordered' => true,
        'striped' => true,
        'condensed' => true,
        'responsive' => true,
        'hover' => true,
        'showPageSummary' => false,
        'persistResize' => false,
    ]);
