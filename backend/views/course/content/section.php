<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 *
 * Created by PhpStorm.
 * User: José Peña
 * Date: 17/1/2017
 * Time: 5:57 PM
 */

use common\helpers\Html;
use yii\grid\GridView;
use yii\data\ActiveDataProvider;

/* @var $model backend\models\Course */
/* @var $section backend\models\Section */

?>

<div class="box box-warning">
    <div class="box-header with-border">
        <h3 class="box-title">
            <i class="fa fa-bookmark" style = "padding-right: 5px;"></i>
            <?= Yii::t('core/school', 'Sections') ?>
        </h3>

        <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
        </div>
    </div>
    <div class="box-body content-sections" style="display: none">

         <?php foreach($model->getSections()->orderBy(['name' => SORT_ASC])->all() as $section) : ?>

            <div class="col-sm-6 content-section" style="padding-right: 5px; padding-left: 5px;">
                <div class="panel panel-default" style="height: 100%; margin-bottom:10px; ">
                    <div class="panel-heading" style="max-height: 50px;">
                        <?= Html::a('<i class="fa fa-folder-open" style="padding-right: 5px;"></i> ' . $section->name,
                            ['/section/update', 'id' => $section->id])
                        ?>
                    </div>

                    <?php if($section->getStudentAssigns()->count()>0): ?>

                        <div class="panel-body" style="padding: 5px; margin-bottom: 0;">
                            <div class="col-md-12 content-content-text">
                                <div class="row">
                                    <?php
                                    echo GridView::widget([
                                        'id' => 'grid-student-section-'.$section->id,
                                        'dataProvider' => new ActiveDataProvider([
                                            'query' => $section->getStudentAssignsInOrder(),
                                        ]),
                                        'summary' => ' ',
                                        'columns' => [
                                            [
                                                'class' => 'yii\grid\SerialColumn',
                                                'contentOptions' => [
                                                    'style' => ['min-width' => '31px']
                                                ],
                                            ],
                                            [
                                                'attribute'=>'student.name',
                                                'contentOptions' => [
                                                    'style' => ['max-width' => '200px']
                                                ],
                                                'format' => 'raw',
                                                'value'=>function ($model) {
                                                    return Html::a($model->student->name, ['/student/update', 'id' => $model->student->id], ['data-pjax' => 0]);
                                                },
                                            ],
                                            [
                                                'attribute'=>'student.dni',
                                                'contentOptions' => [
                                                    'style' => ['max-width' => '170px']
                                                ],
                                                'format' => 'raw',
                                                'value'=>function ($model) {
                                                    return Html::a($model->student->dni, ['/student/update', 'id' => $model->student->id], ['data-pjax' => 0]);
                                                },
                                            ],
                                        ],
                                    ]);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div style="background-color: #e4f1fb;">
                                    <?= Html::a(
                                        '<span class="fa fa-file-excel-o" style = "padding-right: 3px"></span>'. Yii::t('core/school', 'Export to Excel'),
                                        ['course/excel', 'section_id' => $section->id], ['class' => 'btn btn-link btn-group']);
                                    ?>
                                    <?= Html::a(
                                        '<span class="fa fa-file-pdf-o" style = "padding-right: 3px"></span>'. Yii::t('core/school', 'Export to PDF'),
                                        ['course/pdf', 'section_id' => $section->id], ['class' => 'btn btn-link btn-group']);
                                    ?>
                                </div>
                            </div>
                        </div>

                    <?php endif; ?>

                </div>
            </div>

         <?php endforeach; ?>

    </div>
</div>


<?php
$css = <<< CSS
    .grid-view tbody tr td {
        height: 0;
    }
    .grid-view table {
        margin-bottom: 0px;
    }
CSS;

$this->registerCss($css);