<?php

use common\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Course */

$this->title = Yii::t('core/school', 'Update');
$this->params['subtitle'] = Yii::t('core/school', 'Course {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = ['label' => $model->schoolPeriod->name, 'url' => ['/school-period/update', 'id' => $model->schoolPeriod->id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Courses'), ['/course/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
