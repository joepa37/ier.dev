<?php

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\CourseSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;

$this->title = Yii::t('core/school', 'Courses');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['/school-period/index']];
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Sections'), ['/section/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="course-index">
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'grid-course-pjax']) ?>
                </div>
            </div>

            <?=
            GridView::widget([
                'id' => 'grid-course',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActions' => ' ',
                'columns' => [
                    ['class' => '\kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'controller' => '/course',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'title' => function (\backend\models\Course $model) {
                            return Html::a($model->name, ['/course/update', 'id' => $model->id], ['data-pjax' => 0]);
                        },
                        'buttonsTemplate' => '{update}',
                    ],
                    [
                        'attribute' => 'school_period_id',
                        'label' => Yii::t('core/school', 'School Period'),
                        'value' => function($model){
                            return $model->schoolPeriod->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\SchoolPeriod::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-course-search-school_period_id']
                    ],
                    [
                        'attribute' => 'modality_id',
                        'label' => Yii::t('core/school', 'Modality'),
                        'value' => function($model){
                            return $model->curricularMalla->modality->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\Modality::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-course-search-modality_id']
                    ],
                    [
                        'attribute' => 'curricular_malla_id',
                        'label' => Yii::t('core/school', 'Curricular Malla'),
                        'value' => function($model){
                            return $model->curricularMalla->name;
                        },
                        'filterType' => GridView::FILTER_SELECT2,
                        'filter' => \yii\helpers\ArrayHelper::map(\backend\models\CurricularMalla::find()->asArray()->all(), 'id', 'name'),
                        'filterWidgetOptions' => [
                            'pluginOptions' => ['allowClear' => true],
                        ],
                        'filterInputOptions' => ['placeholder' => '', 'id' => 'grid-course-search-curricular_malla_id']
                    ],

                ],
                'pjax' => true,
            ]);
            ?>

        </div>
    </div>
</div>
