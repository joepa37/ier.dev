<?php

use common\helpers\Html;
use  common\widgets\ActiveForm;
use common\Core;

/* @var $this yii\web\View */
/* @var $model backend\models\Course */
/* @var $form common\widgets\ActiveForm */

?>

<div class="course-form">

    <?php
        $form = ActiveForm::begin([
            'id' => 'course-form',
        ])
    ?>

    <div class="row">
        <div class="col-md-9">

            <div class="box box-primary">
                <div class="panel-body">

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form
                                ->field($model, 'name',
                                    [
                                        'hintType' => \common\widgets\ActiveField::HINT_SPECIAL,
                                        'feedbackIcon' => [
                                            'success' => 'ok',
                                            'error' => 'exclamation-sign'
                                        ],
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Html::icon('chevron-right')
                                            ],
                                        ],
                                    ])
                                ->textInput(['maxlength' => true]);
                            ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'school_period_id', ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])
                                ->widget(\kartik\widgets\Select2::classname(),
                                    [
                                        'data' => \yii\helpers\ArrayHelper::map(\backend\models\SchoolPeriod::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                                        'options' => ['placeholder' => Yii::t('core', 'Select...')],
                                        'pluginOptions' => [
                                            'allowClear' => true
                                        ],
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Html::icon('bookmark')
                                            ],
                                        ]
                                    ]); ?>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <?= $form->field(
                                isset($model->curricularMalla->modality) ? $model->curricularMalla->modality : $model,
                                isset($model->curricularMalla->modality) ? 'id' : 'modality_id',
                                ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])
                                ->widget(\kartik\widgets\Select2::classname(), [
                                    'data' => \yii\helpers\ArrayHelper::map(\backend\models\Modality::find()->orderBy('id')->asArray()->all(), 'id', 'name'),
                                    'options' => [
                                        'id'=>'modality_id',
                                        'placeholder' => Yii::t('core', 'Filter...')
                                    ],
                                    'pluginOptions' => [
                                        'allowClear' => true,
                                    ],
                                    'addon' => [
                                        'prepend' => [
                                            'content' => Html::icon('book')
                                        ],
                                    ]
                                ]); ?>
                        </div>
                        <div class="col-md-6">
                            <?= $form->field($model, 'curricular_malla_id', ['hintType' => \common\widgets\ActiveField::HINT_SPECIAL])
                                ->widget(\kartik\depdrop\DepDrop::className(), [
                                    'data' => [
                                        isset($model->curricularMalla) ? $model->curricularMalla->id : null =>
                                            isset($model->curricularMalla) ? $model->curricularMalla->name : null
                                    ],
                                    'type'=>\kartik\depdrop\DepDrop::TYPE_SELECT2,
                                    'options' => [
                                        'id'=>'curricular_malla_id',
                                        'placeholder' => Yii::t('core', 'Select...'),
                                    ],
                                    'select2Options'=>[
                                        'pluginOptions'=>['allowClear'=>true],
                                        'addon' => [
                                            'prepend' => [
                                                'content' => Html::icon('arrow-left')
                                            ],
                                        ]
                                    ],
                                    'pluginOptions' => [
                                        'initialize' =>isset($model->curricularMalla) ? true : false,
                                        'depends'=>['modality_id'],
                                        'url'=>\yii\helpers\Url::to(['/course/curricular-mallas']),
                                        'loadingText' => Yii::t('core', 'Loading...'),
                                    ],
                                ]); ?>
                        </div>
                    </div>

                </div>
            </div>

            <?php if (!$model->isNewRecord): ?>
                <?= $this->render('content/view', [
                    'model' => $model,
                ]) ?>
            <?php endif; ?>

        </div>

        <div class="col-md-3">

            <div class="box box-success">
                <div class="panel-body">
                    <div class="record-info">

                        <?php if (!$model->isNewRecord): ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= $model->createdBy->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_by'] ?> :
                                </label>
                                <span><?= $model->updatedBy->username ?></span>
                            </div>

                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['updated_at'] ?> :
                                </label>
                                <span><?= $model->updatedDatetime ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php else: ?>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_by'] ?> :
                                </label>
                                <span><?= Yii::$app->user->identity->username ?></span>
                            </div>
                            <div class="form-group clearfix">
                                <label class="control-label" style="float: left; padding-right: 5px;">
                                    <?= $model->attributeLabels()['created_at'] ?> :
                                </label>
                                <span><?= "{$model->createdDate} {$model->createdTime}" ?></span>
                            </div>
                            <div class="divider"></div>
                        <?php endif; ?>

                        <div class="form-group">
                            <?php if ($model->isNewRecord): ?>
                                <?= Html::submitButton(Yii::t('core', 'Create'), ['class' => CORE::CREATE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Cancel'), ['/course/index'], ['class' => CORE::CANCEL_CLASS]) ?>
                            <?php else: ?>
                                <?= Html::submitButton(Yii::t('core', 'Save'), ['class' => CORE::SAVE_CLASS]) ?>
                                <?= Html::a(Yii::t('core', 'Delete'), ['/course/delete', 'id' => $model->id], [
                                    'class' => CORE::DELETE_CLASS,
                                    'data' => [
                                        'confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                                        'method' => 'post',
                                    ],
                                ]) ?>
                            <?php endif; ?>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

    <?php ActiveForm::end(); ?>

</div>
