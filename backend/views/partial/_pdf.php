<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Partial */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Partials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partial-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'Partial').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        [
                'attribute' => 'curricularMalla.name',
                'label' => Yii::t('core/school', 'Curricular Malla')
            ],
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerGradeDetailPartial->totalCount){
    $gridColumnGradeDetailPartial = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'gradeDetail.id',
                'label' => Yii::t('core/school', 'Grade Detail')
            ],
                'acumulative_1',
        'acumulative_2',
        'exam',
        'leveling',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGradeDetailPartial,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Grade Detail Partial')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnGradeDetailPartial
    ]);
}
?>
    </div>
</div>
