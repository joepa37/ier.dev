<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model backend\models\Partial */

$this->title = Yii::t('core/school', 'Create Partial');
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Partials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="partial-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
