<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Student */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Student'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="student-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'Student').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        'name',
        'dni',
        'status',
        [
                'attribute' => 'nationality.name',
                'label' => Yii::t('core/school', 'Nationality')
            ],
        'gender_id',
        'birth_day',
        'birth_month',
        'birth_year',
        'pob',
        [
                'attribute' => 'county.name',
                'label' => Yii::t('core/school', 'County')
            ],
        'address',
        'phone_number',
        'email:email',
        [
                'attribute' => 'father.name',
                'label' => Yii::t('core/school', 'Father')
            ],
        [
                'attribute' => 'mother.name',
                'label' => Yii::t('core/school', 'Mother')
            ],
        [
                'attribute' => 'tutor.name',
                'label' => Yii::t('core/school', 'Tutor')
            ],
        'observations',
        'avatar:ntext',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerGradeDetail->totalCount){
    $gridColumnGradeDetail = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'grade.id',
                'label' => Yii::t('core/school', 'Grade')
            ],
                'retrieval',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGradeDetail,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Grade Detail')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnGradeDetail
    ]);
}
?>
    </div>
    
    <div class="row">
<?php
if($providerStudentAssign->totalCount){
    $gridColumnStudentAssign = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'section.name',
                'label' => Yii::t('core/school', 'Section')
            ],
            ];
    echo Gridview::widget([
        'dataProvider' => $providerStudentAssign,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Student Assign')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnStudentAssign
    ]);
}
?>
    </div>
</div>
