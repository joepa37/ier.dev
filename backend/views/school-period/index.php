<?php
//TODO add unique rule to School Period Name and set message for showing on error
//TODO add group addon to title

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\SchoolPeriodSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

use common\Core;
use common\grid\GridPageSize;
use common\grid\GridView;
use common\helpers\Html;
use common\grid\columns\DateRangePicker\DateRangePicker;

$this->title = Yii::t('core/school', 'School Periods');
$this->params['breadcrumbs'][] = $this->title;
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'Courses'), ['/course/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="school-period-index">
    <div class="box box-primary">
        <div class="panel-body">

            <div class="row">
                <div class="col-sm-12 text-right">
                    <?= GridPageSize::widget(['pjaxId' => 'grid-school-period-pjax']) ?>
                </div>
            </div>

            <?=
            GridView::widget([
                'id' => 'grid-school-period',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'bulkActions' => ' ',
                'columns' => [
                    ['class' => '\kartik\grid\SerialColumn'],
                    [
                        'attribute' => 'name',
                        'controller' => '/school-period',
                        'class' => 'common\grid\columns\TitleActionColumn',
                        'title' => function (\backend\models\SchoolPeriod $model) {
                            return Html::a(
                                Yii::t('core/school', 'School Period {name}', [
                                    'name' => $model->name,
                                ]),
                                ['/school-period/update', 'id' => $model->id], ['data-pjax' => 0]
                            );
                        },
                        'buttonsTemplate' => '{update}',
                    ],
                    [
                        'attribute'=>'start_at',
                        'value' => function ($model) {
                            return $model->startAtDate;
                        },
                        'filterType'=> '\common\grid\DatePicker\DateRangePicker',
                        'format'=>'raw',
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['format'=> Yii::$app->settings->get('general.displaydateformat')]
                        ]
                    ],
                    [
                        'attribute'=>'end_at',
                        'value' => function ($model) {
                            return $model->endAtDate;
                        },
                        'filterType'=>GridView::FILTER_DATE,
                        'format'=>'raw',
                        'filterWidgetOptions'=>[
                            'pluginOptions'=>['format'=> Yii::$app->settings->get('general.displaydateformat')]
                        ]
                    ],
                ],
                'pjax' => true,
            ]);
            ?>

        </div>
    </div>
</div>