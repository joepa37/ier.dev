<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 *
 * Created by PhpStorm.
 * User: José Peña
 * Date: 16/1/2017
 * Time: 5:14 PM
 */

/* @var $model backend\models\SchoolPeriod */

?>

<div class="school-period-content">
    <?php
        if($model->getCourses()->count()>0){
            echo $this->render('modality', [
                'model' => $model,
                'modality_id' => 29,
                'modality_name' => backend\models\Modality::modalityName()['MODALITY_29'],
                'color' => 'default',
            ]);
            echo $this->render('modality', [
                'model' => $model,
                'modality_id' => 96,
                'modality_name' => backend\models\Modality::modalityName()['MODALITY_96'],
                'color' => 'danger',
            ]);
            echo $this->render('modality', [
                'model' => $model,
                'modality_id' => 137,
                'modality_name' => backend\models\Modality::modalityName()['MODALITY_137'],
                'color' => 'warning',
            ]);
            echo $this->render('modality', [
                'model' => $model,
                'modality_id' => 102,
                'modality_name' => backend\models\Modality::modalityName()['MODALITY_102'],
                'color' => 'success',
            ]);
        }
    ?>
</div>

<?php
$js = <<< JS
    function resizeWidthOnly(a,b) {
        var c = [window.innerWidth];
            return onresize = function() {
                var d = window.innerWidth,
                e = c.length;
                c.push(d);
                if(c[e]!==c[e-1]){
                    clearTimeout(b);
                    b = setTimeout(a, 0);
                }
        }, a;
    }

    resizeWidthOnly(function() {
        resize();
    });

    $( document ).ready(function() {
        setTimeout( function(){ $(".content-modality").fadeIn( "slow" ); }, 0);
        setTimeout( function(){ resize(); }, 0);
    });

    $(window).resize(function() {
        setTimeout( function(){ resize(); }, 0);
    });

    function resize(){
        var width = $(".content-modality").width();

        if( width > 0 && width < 400 ){
            $(".content-course").removeClass().addClass("content-course col-sm-12");
        }

        if( width > 400 && width < 800 ){
            $(".content-course").removeClass().addClass("content-course col-sm-6");
        }

        if( width > 800 && width < 1200 ){
            $(".content-course").removeClass().addClass("content-course col-sm-4");
        }

        if( width > 1200){
            $(".content-course").removeClass().addClass("content-course col-sm-3");
        }
    }
JS;

$this->registerJs($js,  yii\web\View::POS_END);