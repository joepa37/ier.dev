<?php
/**
 * https://plus.google.com/+joepa37
 * @copyright Copyright (c) 2016 José Peña
 * @license http://www.apache.org/licenses/LICENSE-2.0
 *
 * Created by PhpStorm.
 * User: José Peña
 * Date: 16/1/2017
 * Time: 7:18 PM
 */

use common\helpers\Html;

/* @var $model backend\models\SchoolPeriod */
/* @var $course backend\models\Course */

if ($model->getContainModality($modality_id)): ?>
    <?php echo "<div class='box box-$color'>"; ?>
        <div class="box-header with-border">
            <h3 class="box-title">
                <i class="fa fa-bookmark" style = "padding-right: 5px;"></i>
                <?= $modality_name ?>
            </h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body content-modality" style="display: none">
             <?php foreach($model->getCoursesWhereModality($modality_id)->all() as $course) : ?>
                <div class="col-sm-3 content-course" style="padding-right: 5px; padding-left: 5px;">
                    <div class="panel panel-default" style="height: 100%; margin-bottom:10px; ">
                        <div class="panel-heading" style="max-height: 50px">
                            <?= Html::a('<i class="fa fa-folder-open" style="padding-right: 5px;"></i> ' . $course->name,
                                ['/course/update', 'id' => $course->id])
                            ?>
                        </div>

                        <?php if($course->getSections()->count()>0): ?>
                            <div class="panel-body" style="padding: 5px; margin-bottom: 0;">
                                <div class="col-md-12 content-content-text">
                                    <div class="row">
                                        <table id="card-sections" class="table table-striped table-bordered detail-view" style="margin-bottom: 0;">
                                            <tbody>
                                                <?php foreach($course->getSections()->orderBy(['name' => SORT_ASC])->all() as $section): ?>
                                                    <tr>
                                                        <th><a class="fa fa-chevron-right" style="text-align: center"></a></th>
                                                        <td style="width: 100%">
                                                            <?= Html::a($section->name . ' (' . $section->journey->name.')',
                                                                ['/section/update', 'id' => $section->id])
                                                            ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div style="background-color: #e4f1fb;">
                                        <?= Html::a(
                                            '<span class="fa fa-file-excel-o" style = "padding-right: 3px"></span>'. Yii::t('core/school', 'Export to Excel'),
                                            ['school-period/excel', 'id' => $model->id], ['class' => 'btn btn-link btn-group']);
                                        ?>
                                        <?= Html::a(
                                            '<span class="fa fa-file-pdf-o" style = "padding-right: 3px"></span>'. Yii::t('core/school', 'Export to PDF'),
                                            ['school-period/pdf', 'id' => $model->id], ['class' => 'btn btn-link btn-group']);
                                        ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
             <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>
