<?php

use common\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\SchoolPeriod */

$this->title = Yii::t('core', 'Update');
$this->params['subtitle'] = Yii::t('core/school', 'School Period {name}', [
    'name' => $model->name,
]);
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'School Periods'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
$this->params['buttons'] = [
    Html::a('<i class="fa fa-plus"></i> ' . Yii::t('core', 'Add New'), ['create'], ['class' => common\Core::ADD_NEW_CLASS]),
    Html::a('<i class="fa fa-list"></i> ' . Yii::t('core/School', 'School Periods'), ['/school-period/index'], ['class' => common\Core::LIST_CLASS]),
];
?>
<div class="school-period-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
