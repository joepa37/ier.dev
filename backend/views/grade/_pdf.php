<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use kartik\grid\GridView;

/* @var $this yii\web\View */
/* @var $model backend\models\Grade */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Grades'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="grade-view">

    <div class="row">
        <div class="col-sm-9">
            <h2><?= Yii::t('core/school', 'Grade').' '. Html::encode($this->title) ?></h2>
        </div>
    </div>

    <div class="row">
<?php 
    $gridColumn = [
        ['attribute' => 'id', 'visible' => false],
        [
                'attribute' => 'section.name',
                'label' => Yii::t('core/school', 'Section')
            ],
        'date_range',
    ];
    echo DetailView::widget([
        'model' => $model,
        'attributes' => $gridColumn
    ]); 
?>
    </div>
    
    <div class="row">
<?php
if($providerGradeDetail->totalCount){
    $gridColumnGradeDetail = [
        ['class' => 'yii\grid\SerialColumn'],
        ['attribute' => 'id', 'visible' => false],
                [
                'attribute' => 'student.name',
                'label' => Yii::t('core/school', 'Student')
            ],
        'retrieval',
    ];
    echo Gridview::widget([
        'dataProvider' => $providerGradeDetail,
        'panel' => [
            'type' => GridView::TYPE_PRIMARY,
            'heading' => Html::encode(Yii::t('core/school', 'Grade Detail')),
        ],
        'panelHeadingTemplate' => '<h4>{heading}</h4>{summary}',
        'toggleData' => false,
        'columns' => $gridColumnGradeDetail
    ]);
}
?>
    </div>
</div>
