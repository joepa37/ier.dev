<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\StudentAssign */

$this->title = Yii::t('core/school', 'Update {modelClass}: ', [
    'modelClass' => 'Student Assign',
]) . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('core/school', 'Student Assigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('core/school', 'Update');
?>
<div class="student-assign-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
