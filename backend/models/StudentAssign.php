<?php

namespace backend\models;

use \backend\models\base\StudentAssign as BaseStudentAssign;

/**
 * This is the model class for table "student_assign".
 */
class StudentAssign extends BaseStudentAssign
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['section_id', 'student_id'], 'required'],
            [['section_id', 'student_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ]);
    }
}
