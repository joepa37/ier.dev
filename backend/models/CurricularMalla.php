<?php

namespace backend\models;

use \backend\models\base\CurricularMalla as BaseCurricularMalla;

/**
 * This is the model class for table "curricular_malla".
 */
class CurricularMalla extends BaseCurricularMalla
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'modality_id'], 'required'],
            [['modality_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
}
