<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "class_assign".
 *
 * @property integer $id
 * @property integer $teacher_id
 * @property integer $section_id
 * @property integer $class_table_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \backend\models\Section $section
 * @property \common\models\User $teacher
 * @property \common\models\User $updatedBy
 * @property \backend\models\ClassTable $classTable
 */
class ClassAssign extends \common\db\ActiveRecord implements OwnerAccess
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%class_assign}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['teacher_id', 'section_id', 'class_table_id'], 'required'],
            [['teacher_id', 'section_id', 'class_table_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'teacher_id' => \Yii::t('core/school', 'Teacher ID'),
            'section_id' => \Yii::t('core/school', 'Section ID'),
            'class_table_id' => \Yii::t('core/school', 'Class Table ID'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(\backend\models\Section::className(), ['id' => 'section_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeacher()
    {
        return $this->hasOne(\common\models\User::className(), ['id' => 'teacher_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassTable()
    {
        return $this->hasOne(\backend\models\ClassTable::className(), ['id' => 'class_table_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\ClassAssignQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\ClassAssignQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullClassAssignAccess';
    }
}
