<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "course".
 *
 * @property integer $id
 * @property string $name
 * @property integer $school_period_id
 * @property integer $curricular_malla_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \backend\models\CurricularMalla $curricularMalla
 * @property \backend\models\SchoolPeriod $schoolPeriod
 * @property \common\models\User $updatedBy
 * @property \backend\models\Section[] $sections
 */
class Course extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%course}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'school_period_id', 'curricular_malla_id'], 'required'],
            [['school_period_id', 'curricular_malla_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
            'school_period_id' => \Yii::t('core/school', 'School Period'),
            'modality_id' => \Yii::t('core/school', 'Modality'),
            'curricular_malla_id' => \Yii::t('core/school', 'Curricular Malla'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name for the course'),
            'school_period_id' => \Yii::t('core/school', 'School Period'),
            'modality_id' => \Yii::t('core/school', 'Modality'),
            'curricular_malla_id' => \Yii::t('core/school', 'Curricular Malla'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurricularMalla()
    {
        return $this->hasOne(\backend\models\CurricularMalla::className(), ['id' => 'curricular_malla_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSchoolPeriod()
    {
        return $this->hasOne(\backend\models\SchoolPeriod::className(), ['id' => 'school_period_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(\backend\models\Section::className(), ['course_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\CourseQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\CourseQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullCourseAccess';
    }
}
