<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "journey".
 *
 * @property integer $id
 * @property string $name
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 * @property \backend\models\Section[] $sections
 */
class Journey extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%journey}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSections()
    {
        return $this->hasMany(\backend\models\Section::className(), ['journey_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\JourneyQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\JourneyQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullJourneyAccess';
    }
}
