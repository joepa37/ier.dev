<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "grade".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $date_range
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \backend\models\Section $section
 * @property \common\models\User $updatedBy
 * @property \backend\models\GradeDetail[] $gradeDetails
 */
class Grade extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%grade}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['section_id', 'date_range'], 'required'],
            [['section_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['date_range'], 'string', 'max' => 33]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'section_id' => \Yii::t('core/school', 'Section ID'),
            'date_range' => \Yii::t('core/school', 'Date Range'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(\backend\models\Section::className(), ['id' => 'section_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradeDetails()
    {
        return $this->hasMany(\backend\models\GradeDetail::className(), ['grade_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\GradeQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\GradeQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullGradeAccess';
    }

}
