<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "grade_detail".
 *
 * @property integer $id
 * @property integer $grade_id
 * @property integer $student_id
 * @property integer $retrieval
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \backend\models\Grade $grade
 * @property \backend\models\Student $student
 * @property \common\models\User $updatedBy
 * @property \backend\models\GradeDetailPartial[] $gradeDetailPartials
 */
class GradeDetail extends \common\db\ActiveRecord implements OwnerAccess
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%grade_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade_id', 'student_id'], 'required'],
            [['grade_id', 'student_id', 'retrieval', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'grade_id' => \Yii::t('core/school', 'Grade ID'),
            'student_id' => \Yii::t('core/school', 'Student ID'),
            'retrieval' => \Yii::t('core/school', 'Retrieval'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrade()
    {
        return $this->hasOne(\backend\models\Grade::className(), ['id' => 'grade_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudent()
    {
        return $this->hasOne(\backend\models\Student::className(), ['id' => 'student_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradeDetailPartials()
    {
        return $this->hasMany(\backend\models\GradeDetailPartial::className(), ['grade_detail_id' => 'id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\GradeDetailQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\GradeDetailQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullGradeDetailAccess';
    }
}
