<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "section".
 *
 * @property integer $id
 * @property string $name
 * @property integer $journey_id
 * @property integer $course_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\models\ClassAssign[] $classAssigns
 * @property \backend\models\Grade[] $grades
 * @property \backend\models\Course $course
 * @property \common\models\User $createdBy
 * @property \backend\models\Journey $journey
 * @property \common\models\User $updatedBy
 * @property \backend\models\StudentAssign[] $studentAssigns
 */
class Section extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%section}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'journey_id', 'course_id'], 'required'],
            [['journey_id', 'course_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name'),
            'journey_id' => \Yii::t('core/school', 'Journey'),
            'school_period_id'  => \Yii::t('core/school', 'School Period'),
            'course_id' => \Yii::t('core/school', 'Course'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'name' => \Yii::t('core/school', 'Name for the section'),
            'journey_id' => \Yii::t('core/school', 'Journey'),
            'school_period_id'  => \Yii::t('core/school', 'School Period'),
            'course_id' => \Yii::t('core/school', 'Course'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
    
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClassAssigns()
    {
        return $this->hasMany(\backend\models\ClassAssign::className(), ['section_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrades()
    {
        return $this->hasMany(\backend\models\Grade::className(), ['section_id' => 'id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(\backend\models\Course::className(), ['id' => 'course_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getJourney()
    {
        return $this->hasOne(\backend\models\Journey::className(), ['id' => 'journey_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAssigns()
    {
        return $this->hasMany(\backend\models\StudentAssign::className(), ['section_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStudentAssignsInOrder()
    {
        return $this->hasMany(\backend\models\StudentAssign::className(), ['section_id' => 'id'])
            ->joinWith('student')
            ->orderBy([Student::tableName().'.gender_id' => SORT_ASC, Student::tableName().'.name' => SORT_ASC]);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\SectionQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\SectionQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullSectionAccess';
    }
}
