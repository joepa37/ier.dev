<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;
/**
 * This is the base model class for table "school_period".
 *
 * @property integer $id
 * @property string $name
 * @property integer $start_at
 * @property integer $end_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \backend\models\Course[] $courses
 * @property \common\models\User $createdBy
 * @property \common\models\User $updatedBy
 */
class SchoolPeriod extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%school_period}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'start_at', 'end_at'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('core/school', 'School Period'),
            'name' => Yii::t('core/school', 'Name'),
            'start_at' => Yii::t('core/school', 'Start At'),
            'end_at' => Yii::t('core/school', 'End At'),
            'date_range' => Yii::t('core/school', 'Date Range'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints(){
        return [
            'id' => Yii::t('core/school', 'School Period'),
            'name' => Yii::t('core/school', 'Name for School Period'),
            'start_at' => Yii::t('core/school', 'Start At'),
            'end_at' => Yii::t('core/school', 'End At'),
            'date_range' => Yii::t('core/school', 'Date Range for School Period, when the classes start & end at'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourses()
    {
        return $this->hasMany(\backend\models\Course::className(), ['school_period_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCoursesWhereModality($modality_id)
    {
        return $this->hasMany(\backend\models\Course::className(), ['school_period_id' => 'id'])
            ->joinWith('curricularMalla.modality')
            ->andOnCondition([Modality::tableName().'.id' => $modality_id])
            ->orderBy([Course::tableName().'.curricular_malla_id' => SORT_ASC]);
    }

    public function getContainModality($modality_id){
        if($this->getCoursesWhereModality($modality_id)->count()>0){
            return true;
        }
        /*foreach($this->getCourses()->all() as $course){
            if($course->curricularMalla->modality->id == $modality_id){
                return true;
            }
        }*/
        return false;
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\SchoolPeriodQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\SchoolPeriodQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullSchoolPeriodAccess';
    }
}
