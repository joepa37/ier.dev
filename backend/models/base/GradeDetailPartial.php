<?php

namespace backend\models\base;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\behaviors\BlameableBehavior;
use common\models\OwnerAccess;

/**
 * This is the base model class for table "grade_detail_partial".
 *
 * @property integer $id
 * @property integer $grade_detail_id
 * @property integer $partial_id
 * @property integer $acumulative_1
 * @property integer $acumulative_2
 * @property integer $exam
 * @property integer $leveling
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property \common\models\User $createdBy
 * @property \backend\models\GradeDetail $gradeDetail
 * @property \backend\models\Partial $partial
 * @property \common\models\User $updatedBy
 */
class GradeDetailPartial extends \common\db\ActiveRecord implements OwnerAccess
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%grade_detail_partial}}';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            BlameableBehavior::className(),
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['grade_detail_id', 'partial_id'], 'required'],
            [['grade_detail_id', 'partial_id', 'acumulative_1', 'acumulative_2', 'exam', 'leveling', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('core/school', 'ID'),
            'grade_detail_id' => \Yii::t('core/school', 'Grade Detail ID'),
            'partial_id' => \Yii::t('core/school', 'Partial ID'),
            'acumulative_1' => \Yii::t('core/school', 'Acumulative 1'),
            'acumulative_2' => \Yii::t('core/school', 'Acumulative 2'),
            'exam' => \Yii::t('core/school', 'Exam'),
            'leveling' => \Yii::t('core/school', 'Leveling'),
            'created_by' => Yii::t('core', 'Created By'),
            'created_at' => Yii::t('core', 'Created'),
            'updated_at' => Yii::t('core', 'Updated'),
            'updated_by' => Yii::t('core', 'Updated By'),
        ];
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGradeDetail()
    {
        return $this->hasOne(\backend\models\GradeDetail::className(), ['id' => 'grade_detail_id']);
    }
        
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPartial()
    {
        return $this->hasOne(\backend\models\Partial::className(), ['id' => 'partial_id']);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\GradeDetailPartialQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \backend\models\queries\GradeDetailPartialQuery(get_called_class());
    }

    /**
     *
     * @inheritdoc
     */
    public static function getFullAccessPermission()
    {
        return 'fullGradeDetailPartialAccess';
    }
}
