<?php

namespace backend\models;

use \backend\models\base\State as BaseState;

/**
 * This is the model class for table "state".
 */
class State extends BaseState
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
	
}
