<?php

namespace backend\models;

use \backend\models\base\Partial as BasePartial;

/**
 * This is the model class for table "partial".
 */
class Partial extends BasePartial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'curricular_malla_id'], 'required'],
            [['curricular_malla_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
}
