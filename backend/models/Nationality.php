<?php

namespace backend\models;

use \backend\models\base\Nationality as BaseNationality;

/**
 * This is the model class for table "nationality".
 */
class Nationality extends BaseNationality
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
}
