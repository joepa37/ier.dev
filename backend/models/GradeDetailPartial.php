<?php

namespace backend\models;

use \backend\models\base\GradeDetailPartial as BaseGradeDetailPartial;

/**
 * This is the model class for table "grade_detail_partial".
 */
class GradeDetailPartial extends BaseGradeDetailPartial
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['grade_detail_id', 'partial_id'], 'required'],
            [['grade_detail_id', 'partial_id', 'acumulative_1', 'acumulative_2', 'exam', 'leveling', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer']
        ]);
    }
}
