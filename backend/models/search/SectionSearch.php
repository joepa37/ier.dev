<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Section;

/**
 * backend\models\search\SectionSearch represents the model behind the search form about `backend\models\Section`.
 */
 class SectionSearch extends Section
{
    public $school_period_id;
    public $modality_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'journey_id', 'course_id', 'school_period_id', 'modality_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Section::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => Yii::$app->request->cookies->getValue('_grid_page_size', 20),
            ],
            'sort'=>[
                'defaultOrder'=>['id'=> SORT_DESC],
            ],
        ]);

        $query->joinWith('course.schoolPeriod');
        $query->joinWith('course.curricularMalla.modality');


        $dataProvider->sort->attributes['school_period_id'] = [
            'asc' => ['school_period.name' => SORT_ASC],
            'desc' => ['school_period.name' => SORT_DESC],
        ];
        $dataProvider->sort->attributes['modality_id'] = [
            'asc' => ['modality.name' => SORT_ASC],
            'desc' => ['modality.name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'journey_id' => $this->journey_id,
            'school_period.id' => $this->school_period_id,
            'modality.id' => $this->modality_id,
            'course_id' => $this->course_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'section.name', $this->name]);

        return $dataProvider;
    }
}
