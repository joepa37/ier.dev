<?php

namespace backend\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\ParentOrTutor;

/**
 * backend\models\search\ParentOrTutorSearch represents the model behind the search form about `backend\models\ParentOrTutor`.
 */
 class ParentOrTutorSearch extends ParentOrTutor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nationality_id', 'gender_id', 'birth_day', 'birth_month', 'birth_year', 'county_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name', 'dni', 'pob', 'address', 'phone_number', 'email', 'occupation', 'workplace', 'workplace_phone_number', 'avatar'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ParentOrTutor::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'nationality_id' => $this->nationality_id,
            'gender_id' => $this->gender_id,
            'birth_day' => $this->birth_day,
            'birth_month' => $this->birth_month,
            'birth_year' => $this->birth_year,
            'county_id' => $this->county_id,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'created_by' => $this->created_by,
            'updated_by' => $this->updated_by,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'dni', $this->dni])
            ->andFilterWhere(['like', 'pob', $this->pob])
            ->andFilterWhere(['like', 'address', $this->address])
            ->andFilterWhere(['like', 'phone_number', $this->phone_number])
            ->andFilterWhere(['like', 'email', $this->email])
            ->andFilterWhere(['like', 'occupation', $this->occupation])
            ->andFilterWhere(['like', 'workplace', $this->workplace])
            ->andFilterWhere(['like', 'workplace_phone_number', $this->workplace_phone_number])
            ->andFilterWhere(['like', 'avatar', $this->avatar]);

        return $dataProvider;
    }
}
