<?php

namespace backend\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\models\queries\GradeDetailPartial]].
 *
 * @see \backend\models\queries\GradeDetailPartial
 */
class GradeDetailPartialQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\queries\GradeDetailPartial[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\GradeDetailPartial|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}