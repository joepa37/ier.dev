<?php

namespace backend\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\models\queries\Partial]].
 *
 * @see \backend\models\queries\Partial
 */
class PartialQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\queries\Partial[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\Partial|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}