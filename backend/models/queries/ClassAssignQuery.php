<?php

namespace backend\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\models\queries\ClassAssign]].
 *
 * @see \backend\models\queries\ClassAssign
 */
class ClassAssignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\queries\ClassAssign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\ClassAssign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}