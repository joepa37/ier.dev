<?php

namespace backend\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\models\queries\Section]].
 *
 * @see \backend\models\queries\Section
 */
class SectionQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\queries\Section[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\Section|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}