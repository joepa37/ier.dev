<?php

namespace backend\models\queries;

/**
 * This is the ActiveQuery class for [[\backend\models\queries\StudentAssign]].
 *
 * @see \backend\models\queries\StudentAssign
 */
class StudentAssignQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return \backend\models\queries\StudentAssign[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return \backend\models\queries\StudentAssign|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}