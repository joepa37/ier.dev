<?php

namespace backend\models;

use \backend\models\base\ParentOrTutor as BaseParentOrTutor;

/**
 * This is the model class for table "parent_or_tutor".
 */
class ParentOrTutor extends BaseParentOrTutor
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'dni', 'nationality_id', 'gender_id', 'county_id'], 'required'],
            [['nationality_id', 'gender_id', 'birth_day', 'birth_month', 'birth_year', 'county_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['avatar'], 'string'],
            [['name', 'email'], 'string', 'max' => 255],
            [['dni'], 'string', 'max' => 15],
            [['pob', 'phone_number', 'occupation', 'workplace', 'workplace_phone_number'], 'string', 'max' => 45],
            [['address'], 'string', 'max' => 70]
        ]);
    }
	
}
