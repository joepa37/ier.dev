<?php

namespace backend\models;

use \backend\models\base\Section as BaseSection;

/**
 * This is the model class for table "section".
 */
class Section extends BaseSection
{
    public $school_period_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'journey_id', 'course_id'], 'required'],
            [['journey_id', 'school_period_id', 'course_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ]);
    }

    public static function getCourses($school_period_id){
        $data = Course::find()
            ->where(['school_period_id' => $school_period_id])
            ->select(['id','name'])->asArray()->all();
        return $data;
    }
}
