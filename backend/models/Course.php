<?php

namespace backend\models;

use \backend\models\base\Course as BaseCourse;
use Yii;

/**
 * This is the model class for table "course".
 */
class Course extends BaseCourse
{
    public $modality_id;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'school_period_id', 'curricular_malla_id'], 'required'],
            [['school_period_id', 'modality_id', 'curricular_malla_id', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['created_at', 'updated_at'], 'safe']
        ]);
    }

    public static function getCurricularMallas($modality_id){
        $data = CurricularMalla::find()
            ->where(['modality_id' => $modality_id])
            ->select(['id','name'])->asArray()->all();
        return $data;
    }
}
