<?php

namespace backend\models;

use \backend\models\base\SchoolPeriod as BaseSchoolPeriod;
use Yii;

/**
 * This is the model class for table "school_period".
 */
class SchoolPeriod extends BaseSchoolPeriod
{
    public $date_range;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'date_range', 'start_at', 'end_at'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45],
            [['start_at', 'end_at'], 'safe'],
        ]);
    }

    public function getDateRange(){
        $start = strtotime(date("Y") . '-2-1');
        $end = strtotime(date("Y") . '-11-30');
        return Yii::$app->formatter->asDate($start, 'php:'.Yii::$app->settings->get('general.savedateformat')). ' - ' .
                Yii::$app->formatter->asDate($end, 'php:'.Yii::$app->settings->get('general.savedateformat'));
    }

    public function getMinYear(){
        return '2001';
    }


    public function getMaxYear(){
        return date('Y', strtotime('+1 year'));
    }

    public function getActualYear(){
        return date('Y');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStartAtDate()
    {
        return Yii::$app->formatter->asDate($this->start_at);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEndAtDate()
    {
        return Yii::$app->formatter->asDate($this->end_at);
    }
}
