<?php

namespace backend\models;

use \backend\models\base\Journey as BaseJourney;

/**
 * This is the model class for table "journey".
 */
class Journey extends BaseJourney
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
}
