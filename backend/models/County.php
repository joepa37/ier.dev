<?php

namespace backend\models;

use \backend\models\base\County as BaseCounty;

/**
 * This is the model class for table "county".
 */
class County extends BaseCounty
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name', 'state_id'], 'required'],
            [['state_id'], 'integer'],
            [['name'], 'string', 'max' => 45]
        ]);
    }
	
}
