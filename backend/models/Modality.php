<?php

namespace backend\models;

use \backend\models\base\Modality as BaseModality;
use Yii;

/**
 * This is the model class for table "modality".
 */
class Modality extends BaseModality
{

    const MODALITY_96 = 96;
    const MODALITY_29 = 29;
    const MODALITY_102 = 102;
    const MODALITY_137 = 137;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['name'], 'required'],
            [['created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 70]
        ]);
    }

    /**
     * @inheritdoc
     * self::modalityName()['MODALITY_96']
     */
    public static function modalityName()
    {
        return [
            'MODALITY_96' => Yii::t('core/school', '(96) - BACHILLERATO EN CIENCIAS Y LETRAS Y TÉCNICO EN COMPUTACIÓN'),
            'MODALITY_29' => Yii::t('core/school', '(29) - CICLO COMÚN'),
            'MODALITY_102' => Yii::t('core/school', '(102) - BACHILLERATO TÉCNICO PROFESIONAL EN INFORMÁTICA'),
            'MODALITY_137' => Yii::t('core/school', '(137) - TERCER CICLO DE EDUCACIÓN BÁSICA'),
        ];
    }
}
