<?php

namespace backend\models;

use \backend\models\base\Student as BaseStudent;

/**
 * This is the model class for table "student".
 */
class Student extends BaseStudent
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_replace_recursive(parent::rules(),
	    [
            [['id', 'name', 'dni', 'nationality_id', 'gender_id', 'county_id'], 'required'],
            [['id', 'status', 'nationality_id', 'gender_id', 'birth_day', 'birth_month', 'birth_year', 'county_id', 'father_id', 'mother_id', 'tutor_id', 'created_at', 'updated_at', 'created_by', 'updated_by'], 'integer'],
            [['avatar'], 'string'],
            [['name', 'email', 'observations'], 'string', 'max' => 255],
            [['dni'], 'string', 'max' => 15],
            [['pob', 'phone_number'], 'string', 'max' => 45],
            [['address'], 'string', 'max' => 70]
        ]);
    }
	
}
