<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'backend',
    'homeUrl' => '/admin',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [
        'frontend' => [
            'class' => 'common\frontend\Module',
        ],
        'admin' => [
            'class' => 'common\admin\Module',
        ],
        'settings' => [
            'class' => 'common\settings\SettingsModule',
        ],
        'menu' => [
            'class' => 'common\menu\MenuModule',
        ],
        'translation' => [
            'class' => 'common\translation\TranslationModule',
        ],
        'user' => [
            'class' => 'common\user\UserModule',
        ],
        'media' => [
            'class' => 'common\media\MediaModule',
            'routes' => [
                'baseUrl' => '', // Base absolute path to web directory
                'basePath' => '@frontend/web', // Base web directory url
                'uploadPath' => 'uploads', // Path for uploaded files in web directory
            ],
        ],
        'post' => [
            'class' => 'common\post\PostModule',
        ],
        'page' => [
            'class' => 'common\page\PageModule',
        ],
        'seo' => [
            'class' => 'common\seo\SeoModule',
        ],
        'comment' => [
            'class' => 'common\comment\CommentModule',
        ],
    ],
    'components' => [
        'request' => [
            'baseUrl' => '/admin',
        ],
        'urlManager' => [
            'class' => 'common\web\MultilingualUrlManager',
            'showScriptName' => false,
            'enablePrettyUrl' => true,
            'multilingualRules' => false,
            'rules' => array(
                //add here local frontend controllers
                '<controller:(test)>' => '<controller>/index',
                '<controller:(test)>/<id:\d+>' => '<controller>/view',
                '<controller:(test)>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:(test)>/<action:\w+>' => '<controller>/<action>',
            )
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
    ],
    'params' => $params,
];
