<?php

namespace backend\controllers;

use Yii;
use common\controllers\admin\DashboardController;
use yii\web\Response;

class SiteController extends DashboardController
{
    public $freeAccessActions = ['sidebar'];
    /**
     * Save the sidebar status.
     *
     * @return mixed
     */
    //todo, add a permission to migration file
    public function actionSidebar()
    {
        if (Yii::$app->request->isAjax) {
            $sidebar = Yii::$app->request->post()['sidebar'];
            Yii::$app->user->settings->set('sidebar-status', $sidebar);
        }else{
            return $this->redirect($this->getRedirectPage('index'));
        }
    }
}