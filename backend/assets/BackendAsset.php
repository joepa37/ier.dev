<?php
/**
 * Created by PhpStorm.
 * User: José Peña
 */

namespace backend\assets;

use common\assets\core\CoreAsset;
use common\assets\pace\PaceAsset;
use common\auth\assets\AvatarAsset;
use yii\web\AssetBundle;

class BackendAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';

    public $css = [
    ];
    public $js = [
    ];

    public $depends = [
        CoreAsset::class,
        PaceAsset::class,
        AvatarAsset::class,
    ];
}
