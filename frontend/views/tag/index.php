<?php

use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = '#' . $tag->titleTranslation;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tag-index">
    <div class="box box-primary">
        <div class="panel-body">
            <?php /* @var $post common\post\models\Post */ ?>
            <?php foreach ($posts as $post) : ?>
                <?= $this->render('/items/post.php', ['post' => $post, 'page' => 'tag']) ?>
            <?php endforeach; ?>

            <div class="text-center">
                <?= LinkPager::widget(['pagination' => $pagination]) ?>
            </div>
        </div>
    </div>
</div>
