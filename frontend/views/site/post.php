<?php
/* @var $this yii\web\View */

use common\comments\widgets\Comments;
use common\post\models\Post;

/* @var $post common\post\models\Post */

$this->title = $post->titleTranslation;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="post">
    <div class="box box-primary">
        <div class="panel-body">
            <?= $this->render('/items/post.php', ['post' => $post]) ?>
        </div>
    </div>
    <?php if ($post->comment_status == Post::COMMENT_STATUS_OPEN): ?>
        <?php echo Comments::widget(['model' => Post::className(), 'model_id' => $post->id]); ?>
    <?php endif; ?>
</div>
