<?php

use yii\widgets\LinkPager;

/* @var $this yii\web\View */

$this->title = 'Homepage';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-index">
    <div class="box box-primary">
        <div class="panel-body">
            <?php if (!Yii::$app->user->getIsGuest()) : ?>
                <div class="jumbotron">
                    <h1>Welcome!</h1>

                    <p class="lead">Teacher, this is your application to manage grades.</p>

                    <?php if(\common\models\User::canRoute(['/admin'])): ?>
                        <p><a class="btn btn-lg btn-success" href="/admin">Administration area</a></p>
                    <?php endif; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>

    <h2><?=Yii::t('core/post', 'Posts')?></h2>

    <div class="row">
        <div class="col-md-3">
            <?= \common\comment\widgets\RecentComments::widget() ?>
        </div>
        <div class="col-md-9">
            <div class="box box-success">
                <div class="panel-body">
                    <?php /* @var $post common\post\models\Post */ ?>
                    <?php foreach ($posts as $post) : ?>
                        <?= $this->render('/items/post.php', ['post' => $post, 'page' => 'index']) ?>
                    <?php endforeach; ?>
                    <div class="text-center">
                        <?= LinkPager::widget(['pagination' => $pagination]) ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
