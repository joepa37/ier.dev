<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

//$this->title = Yii::t('core', 'About');
$this->params['breadcrumbs'][] = Yii::t('core', 'About');
?>
<div class="site-about">
    <div class="box box-primary">
        <div class="panel-body text-center">
            <?php
                $bundle = \frontend\assets\FrontendAsset::register($this);
                $logo = $bundle->baseUrl . '/img/IER School - about.png';
                $options = [
                    'style' => ['width' => '100%', 'height' => 'auto'],
                    'alt' => 'IER School'
                ];
                echo \common\helpers\Html::img($logo, $options);
            ?>
        </div>
    </div>
</div>
